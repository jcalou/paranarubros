<div class="row">
  <div class="large-12 columns">
    <h1>Agregar nuevo usuario</h1>
    <?= validation_errors(); ?>
    <form action="<?php echo base_url('admin/agregar_usuario'); ?>" method="POST">
      <div class="row">
        <div class="large-2 columns">
          <label for="nombre">Nombre:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="nombre" value="" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="email">Email:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="email" value="" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="password">Password:</label>
        </div>
        <div class="large-8 columns">
          <input type="password" name="password" value="" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="tel_fijo">Tel&eacute;fono Fijo:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="tel_fijo" value="" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="tel_movil">Tel&eacute;fono M&oacute;vil:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="tel_movil" value="" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="domicilio">Domicilio:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="domicilio" value="" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="localidad">Localidad:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="localidad" value="" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="cod_post">C&oacute;digo postal:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="cod_post" value="" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="provincia">Provincia:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="provincia" value="" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-10 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" class="button right" value="Crear nuevo usuario"/>
        </div>
      </div>
    </form>
  </div>
</div>
