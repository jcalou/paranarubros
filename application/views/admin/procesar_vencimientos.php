<p>Procesando Vencimientos...</p>
<p>Este proceso busca las publicaciones con fecha de vencimiento anterior al dia de hoy que no esten vencidas y las pasa al estado "vencido"</p>
<?php $hoy = date('Y-m-d'); ?>
<br>
<?php 
echo "-------------------------------------------- Inicio del proceso ----------------------------------------------<br>";
foreach($publicaciones as $publicacion):

  if($publicacion->valido_hasta < $hoy && $publicacion->estado != "vencido"){
    echo $publicacion->id . "&nbsp;-&nbsp";
    echo "vencido". "&nbsp;-&nbsp";
    echo $publicacion->valido_hasta . "<br>";
    $model_publicaciones->cambiar_estado($publicacion->id,"vencido");
  } 

endforeach;
echo "<br>---------------------------------------------- Fin del proceso -----------------------------------------------";
?>