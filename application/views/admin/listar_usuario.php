<div class="row">
    <div class="large-12 columns">
        <h1>Listado de usuarios</h1>
        <table>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Tel. Fijo</th>
                <th>Tel. M&oacute;vil</th>
                <th>Domicilio</th>
                <th>Localidad</th>
                <th>C.P.</th>
                <th>Provincia</th>
                <th>Acciones</th>
            </tr>
            <?php foreach($usuarios as $usuario):?>
            <tr>
                <td><?= $usuario->id?></td>
                <td><?= $usuario->nombre?></td>
                <td><?= $usuario->email?></td>
                <td><?= $usuario->tel_fijo?></td>
                <td><?= $usuario->tel_movil?></td>
                <td><?= $usuario->domicilio?></td>
                <td><?= $usuario->localidad?></td>
                <td><?= $usuario->cod_post?></td>
                <td><?= $usuario->provincia?></td>
                <td>
                    <a href="<?= base_url('admin/editar_usuario/'.$usuario->id) ?>">Editar</a> |
                    <a href="#" data-user-id="<?= $usuario->id ?>" class="confirm" data-tooltip="¿Esta seguro?">Eliminar</a>
                </td>
            </tr>
            <?php endforeach;?>
        </table>
        <p><?php echo $links; ?></p>
        <a href="<?php echo base_url('admin/agregar_usuario'); ?>" class="button">Agregar nuevo usuario</a>
    </div>
</div>


