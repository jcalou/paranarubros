<div class="contentb">
  <div id="container">
    <h1>Editar datos personales</h1>
    <div class="form_errors">
      <?php echo validation_errors(); ?>
    </div>
    <form action="<?php echo base_url('usuario/editar') ?>" method="POST">
      <label for="buscar">Nombre:</label>
      <input type="text" name="nombre" value="<?= set_value('nombre',$usuario_logueado['nombre']);?>" />
      <div class="info">Nombre completo</div>
      <br/>
      <label for="buscar">Email:</label>
      <input type="text" name="email" value="<?= set_value('email',$usuario_logueado['email']);?>" />
      <div class="info">Email</div>
      <br/>
      <label for="buscar">Contrase&ntilde;a:</label>
      <input type="password" name="password" value="" />
      <div class="info">Nueva Contrase&ntilde;a</div>
      <br/>
      <label for="buscar">Tel&eacute;fono Fijo:</label>
      <input type="text" name="tel_fijo" value="<?= set_value('tel_fijo',$usuario_logueado['tel_fijo']);?>" />
      <div class="info">Tel&eacute;fono Fijo</div>
      <br/>
      <label for="buscar">Tel&eacute;fono M&oacute;vil:</label>
      <input type="text" name="tel_movil" value="<?= set_value('tel_movil',$usuario_logueado['tel_movil']);?>" />
      <div class="info">Tel&eacute;fono M&oacute;vil</div>
      <br/>
      <label for="buscar">Domicilio:</label>
      <input type="text" name="domicilio" value="<?= set_value('domicilio',$usuario_logueado['domicilio']);?>" />
      <div class="info">Domicilio</div>
      <br/>
      <label for="buscar">Localidad:</label>
      <input type="text" name="localidad" value="<?= set_value('localidad',$usuario_logueado['localidad']);?>" />
      <div class="info">Localidad</div>
      <br/>
      <label for="buscar">C&oacute;digo Postal:</label>
      <input type="text" name="cod_post" value="<?= set_value('cod_post',$usuario_logueado['cod_post']);?>" />
      <div class="info">C&oacute;digo Postal</div>
      <br/>
      <label for="buscar">Provincia:</label>
      <input type="text" name="provincia" value="<?= set_value('provincia',$usuario_logueado['provincia']);?>" />
      <div class="info">Provincia</div>
      <br/>
      <input type="hidden" name="post" value="1" />
      <input type="submit" value="Guardar"/>
    </form>
    <p>&nbsp;</p>
  </div>
</div>
