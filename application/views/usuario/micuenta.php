<div class="contentb">
  <div id="container">
    <h1>Mi Cuenta</h1>
    <hr />
    <p><a href="<?php echo base_url('usuario/publicar'); ?>"><i class="fa fa-plus-square"></i> Publicar un aviso</a></p>
    <p><a href="<?php echo base_url('usuario/editar'); ?>"><i class="fa fa-pencil-square-o"></i> Editar Datos Personales</a></p>
    <p><a href="<?php echo base_url('usuario/mispublicaciones'); ?>"><i class="fa fa-list"></i> Mis Publicaciones</a></p>

    <hr />
    <p><a href="<?php echo base_url('usuario/logout'); ?>"><i class="fa fa-sign-out"></i> Cerrar Sesi&oacute;n (Logout)</a></p>
  </div>
</div>
