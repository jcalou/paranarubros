<div class="contentb">
  <div id="container">
    <h1>Mis publicaciones</h1>
    <?php foreach ($publicaciones as $item){ ?>
    <hr>
    <div class="mispublicaciones-item <?php if ($item->estado=="activo") {echo "activo";} ?>">
        <div class="img" id="img<?php echo $item->id ?>"></div>
        <style>#img<?php echo $item->id ?>{background-image:url(<?php echo base_url('assets/uploads/' . $item->nombre) ?>);}</style>
        <div class="texto">
            <h2><?php echo $item->titulo ?></h2>
            <p><?php echo $item->descripcion ?></p>
            <p>Estado: <?php echo $item->estado ?></p>
            <?php if ($item->estado == "Activo") {?>
            <p>V&aacute;lido hasta: <?php echo date('d/m/Y',strtotime($item->valido_hasta));?></p>
            <?php } ?>
        </div>
        <div class="opciones">
            <?php if ($item->estado == "pendiente") {?>
            <p><a href="<?= base_url('usuario/editar_publicacion/'.$item->id); ?>"><i class="fa fa-pencil-square-o"></i> Editar</a></p>
            <p><a href="<?= base_url('usuario/publicar_imagenes/'.$item->id); ?>"><i class="fa fa-image"></i> Imagenes</a></p>
            <p><a href="<?= base_url('usuario/pagar_publicacion/'.$item->id) ?>"><i class="fa fa-credit-card"></i> Pagar</a></p>
            <p><a href="<?= base_url('usuario/eliminar_publicacion/'.$item->id) ?>" onclick="return confirm('¿Esta seguro que desea borrar esta publicacion?');"><i class="fa fa-times"></i> Eliminar</a></p>
            <?php } ?>
            <?php if ($item->estado == "procesando_pago") {?>
            <p><a href="<?= base_url('usuario/editar_publicacion_activo/'.$item->id); ?>"><i class="fa fa-pencil-square-o"></i> Editar</a></p>
            <p><a href="<?= base_url('usuario/publicar_imagenes/'.$item->id); ?>"><i class="fa fa-image"></i> Imagenes</a></p>
            <p><a href="<?= base_url('usuario/eliminar_publicacion/'.$item->id) ?>" onclick="return confirm('¿Esta seguro que desea borrar esta publicacion?');"><i class="fa fa-times"></i> Eliminar</a></p>
            <?php } ?>
            <?php if ($item->estado == "activo") {?>
            <p><a href="<?= base_url('usuario/editar_publicacion_activo/'.$item->id); ?>"><i class="fa fa-pencil-square-o"></i> Editar</a></p>
            <p><a href="<?= base_url('usuario/publicar_imagenes/'.$item->id); ?>"><i class="fa fa-image"></i> Imagenes</a></p>
            <p><a href="<?= base_url('usuario/ocultar_publicacion/'.$item->id) ?>" onclick="return confirm('¿Esta seguro que desea ocultar esta publicacion? No se mostrara en los listados');"><i class="fa fa-pause"></i> Ocultar</a></p>
            <?php } ?>
            <?php if ($item->estado == "inactivo") {?>
            <p><a href="<?= base_url('usuario/editar_publicacion/'.$item->id); ?>"><i class="fa fa-pencil-square-o"></i> Editar</a></p>
            <p><a href="<?= base_url('usuario/publicar_imagenes/'.$item->id); ?>"><i class="fa fa-image"></i> Imagenes</a></p>
            <p><a href="<?= base_url('usuario/mostrar_publicacion/'.$item->id) ?>"><i class="fa fa-play"></i> Mostrar</a></p>
            <p><a href="<?= base_url('usuario/eliminar_publicacion/'.$item->id) ?>" onclick="return confirm('¿Esta seguro que desea borrar esta publicacion?');"><i class="fa fa-times"></i> Eliminar</a></p>
            <?php } ?>
            <?php if ($item->estado == "vencido") {?>
            <p><a href="<?= base_url('usuario/editar_publicacion/'.$item->id); ?>"><i class="fa fa-pencil-square-o"></i> Editar</a></p>
            <p><a href="<?= base_url('usuario/publicar_imagenes/'.$item->id); ?>"><i class="fa fa-image"></i> Imagenes</a></p>
            <p><a href="<?= base_url('usuario/pagar_publicacion/'.$item->id) ?>"><i class="fa fa-credit-card"></i> Pagar</a></p>
            <p><a href="<?= base_url('usuario/eliminar_publicacion/'.$item->id) ?>" onclick="return confirm('¿Esta seguro que desea borrar esta publicacion?');"><i class="fa fa-times"></i> Eliminar</a></p>
            <?php } ?>
        </div>
    </div>

    <?php } ?>
  </div>
</div>
