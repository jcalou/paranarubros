<div class="content-buscar">
  <div id="container">
    <h1>Formulario de b&uacute;squeda</h1>
    <div class="form_errors">
      <?php echo validation_errors(); ?>
    </div>
    <form action="<?php echo base_url('buscar') ?>" method="POST">
      <label for="buscar">Buscar:</label>
      <input type="text" name="buscar" id="buscar" />
      <div class="info">Ingrese el texto a buscar</div>
      <br/>
      <label for="categoria">Categor&iacute;a:</label>
      <?php echo categoria_select(); ?>
      <div class="info">Seleccione una categor&iacute;a en la cual buscar</div>
      <br/>
      <input type="hidden" name="post" value="1" />
      <input type="submit" value="Buscar"/>
    </form>
    <p>&nbsp;</p>
  </div>
</div>