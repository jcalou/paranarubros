<div class="content-home">
  <div id="container">
    <div class="item-row-banner">
      <a href="<?php echo base_url('usuario/registro'); ?>">Registrate</a>
    </div>
    <?php
    $q = 0;
    foreach($items as $item){
      $q += 1;
      if ($q == 1) {echo "<div class='item-row'>";} ?>
      <a class="item" href="<?php echo base_url('item/'.$item->id); ?>">
        <div class="img" id="img<?php echo $item->id ?>"></div>
        <style>#img<?php echo $item->id ?>{background-image:url(<?php echo base_url('assets/uploads/'.$item->nombre_imagen) ?>);}</style>
        <h2><?php echo $item->titulo ?></h2>
        <!--p>Id de la publicaci&oacute;n: <?php echo $item->id ?></p-->
      </a>
    <?php
    if ($q == 4) {echo "</div>";$q = 0;};
    }
    if ($q != 0) {echo "</div>";};
    ?>
  </div>
</div>
