<div class="contentb">
  <div id="container">
    <h1>Publicar un aviso</h1>
    <p>Para publicar un aviso debe completar los 4 pasos.</p>
    <div class="form_errors">
      <?php echo validation_errors(); ?>
    </div>
    <form action="<?php echo base_url('usuario/publicar') ?>" method="POST" id="publicar-form">
      <div id="wizard">
        <h2>Tus datos personales</h2>
        <section>
          <h4>Tus datos personales</h4>
          <hr />
          <p>Seleccion&aacute; cu&aacute;l de los siguientes datos deseas incluir en la publicaci&oacute;n.</p>
          <?php
          if (isset($usuario_logueado['nombre']) && $usuario_logueado['nombre'] != null){echo "<div class='checkboxwrapper'><p><b>Nombre:</b> ".$usuario_logueado['nombre']."</p></div>";}
          if (isset($usuario_logueado['email']) && $usuario_logueado['email'] != null){echo "<div class='checkboxwrapper'><p><b>Email:</b> ".$usuario_logueado['email']."<input name='pubmail' type='checkbox' id='pubmail' value='true' /><span class='form-check-text'>Deseo que se muestre mi email en la publicaci&oacute;n.</span></p></div>";}
          if (isset($usuario_logueado['tel_fijo']) && $usuario_logueado['tel_fijo'] != null){echo "<div class='checkboxwrapper'><p><b>Tel&eacute;fono Fijo:</b> ".$usuario_logueado['tel_fijo']."<input name='pubtel_fijo' type='checkbox' id='pubtel_fijo' value='true' /><span class='form-check-text'>Deseo que se muestre mi tel&eacute;fono fijo en la publicaci&oacute;n.</span></p></div>";}
          if (isset($usuario_logueado['tel_movil']) && $usuario_logueado['tel_movil'] != null){echo "<div class='checkboxwrapper'><p><b>Tel&eacute;fono M&oacute;vil:</b> ".$usuario_logueado['tel_movil']."<input name='pubtel_movil' type='checkbox' id='pubtel_movil' value='true' /><span class='form-check-text'>Deseo que se muestre mi tel&eacute;fono m&oacute;vil en la publicaci&oacute;n.</span></p></div>";}
          if (isset($usuario_logueado['domicilio']) && $usuario_logueado['domicilio'] != null){echo "<div class='checkboxwrapper'><p><b>Domicilio:</b> ".$usuario_logueado['domicilio']."<input name='pubdomicilio' type='checkbox' id='pubdomicilio' value='true' /><span class='form-check-text'>Deseo que se muestre mi domicilio en la publicaci&oacute;n.</span></p></div>";}
          if (isset($usuario_logueado['localidad']) && $usuario_logueado['localidad'] != null){echo "<div class='checkboxwrapper'><p><b>Localidad:</b> ".$usuario_logueado['localidad']."</p></div>";}
          if (isset($usuario_logueado['cod_post']) && $usuario_logueado['cod_post'] != null){echo "<div class='checkboxwrapper'><p><b>C&oacute;digo postal:</b> ".$usuario_logueado['cod_post']."</p></div>";}
          if (isset($usuario_logueado['provincia']) && $usuario_logueado['provincia'] != null){echo "<div class='checkboxwrapper'><p><b>Provincia:</b> ".$usuario_logueado['provincia']."</p></div>";}
          ?>
          <br/>
        </section>

        <h2>Datos de la publicaci&oacute;n</h2>
        <section>
           <h4>Datos de la publicaci&oacute;n</h4>
           <hr />
          <div class="rowform">
            <label for="categoria">Categor&iacute;a:</label>
            <?php echo categoria_select("required"); ?>
            <div class="info">Seleccione una categor&iacute;a en la cual buscar</div>
          </div>
          <div class="rowform">
            <label for="titulo">T&iacute;tulo:</label>
            <input type="text" size="20" id="titulo" name="titulo" class="required"/>
            <div class="info">Ingrese el t&iacute;tulo para la publicaci&oacute;n.</div>
          </div>
          <div class="rowform">
            <label for="descripcion">Descripci&oacute;n:</label>
            <textarea name="descripcion" id="descripcion" maxlength="400" cols="30" rows="10"></textarea>
            <div class="info">Ingrese la descripci&oacute;n para la publicaci&oacute;n. M&aacute;ximo 400 caracteres.</div>
          </div>
          <div class="rowform">
            <label for="precio">Precio:</label>
            <input type="text" size="20" id="precio" name="precio" value="$" />
            <div class="info">Ingrese el precio que desea aplicarle al producto publicado.</div>
          </div>
          <div class="rowform">
            <label for="formapago">Forma de pago:</label>
            <input type="text" size="20" id="formapago" name="formapago" placeholder="Contado, cuotas, a convenir..." />
            <div class="info">Ingrese el forma de pago para la publicaci&oacute;n.</div>
          </div>
          <div class="rowform">
            <label for="comentario">Comentarios adicionales:</label>
            <textarea name="comentario" id="comentario" maxlength="400" cols="30" rows="10"></textarea>
            <div class="info">Ingrese comentarios adicionales que quisiera incluir en la publicaci&oacute;n. M&aacute;ximo 400 caracteres.</div>
          </div>
          <div class="rowform">
            <label for="publicarpor">Publicar por:</label>
            <select name="publicarpor" id="publicarpor">
              <option value="30">30 d&iacute;as - $25</option>
              <option value="60">60 d&iacute;as - $50</option>
              <option value="90">90 d&iacute;as - $75</option>
              <option value="120">120 d&iacute;as - $100</option>
              <option value="150">150 d&iacute;as - $125</option>
            </select>
            <div class="info">Vence el <?php echo date('d/m/Y', strtotime("+30 days")); ?></div>
            <input type="hidden" name="valido_hasta" id="valido_hasta" value="<?php echo date('Y-m-d', strtotime("+30 days")); ?>">
          </div>
        </section>

        <h2>Fin del proceso</h2>
        <section>
          <h4>Fin del proceso</h4>
          <hr />
          <p>Al clickear en el bot&oacute;n CARGAR IMAGEN se procedera a guardar la publicaci&oacute;n y luego de eso se redireccionar&aacute; al formulario para cargar las imagenes.</p>
          <p>Una vez completado ese paso se podr&aacute; proceder al pago de la publicaci&oacute;n desde el bot&oacute;n de pago en MIS PUBLICACIONES.</p>
        </section>
      </div>
      <input type="hidden" name="post" value="1" />
    </form>
  </div>
</div>
