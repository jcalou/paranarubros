<?php
class categorias_model extends CI_Model
{
    var $nombre = "";
    var $url = "";

    public function __construct()
    {
        parent::__construct();
    }
    //vamos a cargar todos los usuarios
    public function get_categorias()
    {
        $sql = $this->db->query('SELECT c.id,c.*, COUNT( p.id ) as cantidad
        FROM categoria c
        LEFT JOIN publicacion p ON c.id = p.id_categoria
        GROUP BY c.id');
        return $sql->result();
    }

}
