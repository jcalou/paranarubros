<div class="contentb">
  <div id="container">
    <h1>Login</h1>
    <div class="form_errors">
      <?php echo validation_errors(); ?>
    </div>
    <form action="<?php echo base_url('usuario/login') ?>" method="POST">
      <label for="email">Email:</label>
      <input type="text" size="20" id="email" name="email"/>
      <div class="info">Ingrese el email con el que esta registrado</div>
      <br/>
      <label for="password">Password:</label>
      <input type="password" size="20" id="passowrd" name="password"/>
      <div class="info">Ingrese la contraseña</div>
      <input type="hidden" name="post" value="1" />
      <br/>
      <input type="submit" value="Login"/>
    </form>
    <p class="form-bottom-link">¿No estas registrado? <a href="<?php echo base_url('usuario/registro') ?>">Registrate ac&aacute;</a></p>
  </div>
</div>
