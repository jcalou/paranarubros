<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/foundation.css') ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/darktooltip.css') ?>" />
    <script src="<?php echo base_url('assets/js/vendor/modernizr-2.6.2.min.js') ?>"></script>
  </head>
  <body>
    <nav class="top-bar" data-topbar>
      <ul class="title-area">
        <li class="name">
          <h1><a href="<?php echo base_url('admin/dashboard'); ?>">Paranarubros Admin</a></h1>
        </li>
      </ul>

      <section class="top-bar-section">
        <!-- Right Nav Section -->
        <ul class="right">
          <li class="active"><a href="<?php echo base_url('admin/logout'); ?>">Logout</a></li>
        </ul>

        <!-- Left Nav Section -->
        <ul class="left">
          <li class="has-dropdown">
            <a href="<?php echo base_url('admin/listado_usuarios'); ?>">Usuarios</a>
            <ul class="dropdown">
              <li><a href="<?php echo base_url('admin/listado_usuarios'); ?>">Listado de usuarios</a></li>
              <li><a href="<?php echo base_url('admin/agregar_usuario'); ?>">Agregar nuevo usuario</a></li>
            </ul>
          </li>
          <li class="has-dropdown">
            <a href="<?php echo base_url('admin/listado_publicaciones/main'); ?>">Listado de Publicaciones</a>
            <ul class="dropdown">
              <li><a href="<?php echo base_url('admin/listado_publicaciones/pendiente'); ?>">Pendientes</a></li>
              <li><a href="<?php echo base_url('admin/listado_publicaciones/procesando_pago'); ?>">Procesando Pago</a></li>
              <li><a href="<?php echo base_url('admin/listado_publicaciones/activo'); ?>">Activos</a></li>
              <li><a href="<?php echo base_url('admin/listado_publicaciones/inactivo'); ?>">Inactivos</a></li>
              <li><a href="<?php echo base_url('admin/listado_publicaciones/vencido'); ?>">Vencidos</a></li>
            </ul>
          </li>
          <li><a href="<?php echo base_url('admin/procesar_vencimientos'); ?>">Procesar Vencimientos</a></li>
        </ul>
      </section>
    </nav>
