<div class="content-home">
  <div id="container">
    <h1><?php echo $item['titulo']; ?></h1>
    <h3>Categor&iacute;a: <a href="<?php echo base_url('categoria/'.$item['url_categoria']); ?>"><?php echo $item['nombre_categoria']; ?></a></h3>
    <hr>
    <div class="images-detail">
    <?php
    $q = 0;
    foreach($imagenes as $imagen){
      $q += 1;
      ?>
    <a href="<?php echo base_url('assets/uploads/'.$imagen['nombre']) ?>" data-lightbox="roadtrip">
      <div class="imgdetail img<?php echo $q;?>"></div>
      <style>.img<?php echo $q;?>{background-image:url(<?php echo base_url('assets/uploads/'.$imagen['nombre']) ?>);}</style>
    </a>
    <?php }?>
    </div>
    <hr>
    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
        <tbody>
            <tr>
                <td colspan="" rowspan="" headers="" style="width: 50%;vertical-align: top;">
                    <h3>Informaci&oacute;n de la publicaci&oacute;n</h3>
                    <?php if (isset($item['descripcion']) && $item['descripcion'] != "") {?>
                    <p><b>Descripcion</b>: <?php echo $item['descripcion']; ?></p>
                    <?php } ?>
                    <?php if (isset($item['precio']) && $item['precio'] != "") {?>
                    <p><b>Precio</b>: <?php echo $item['precio']; ?></p>
                    <?php } ?>
                    <?php if (isset($item['forma_de_pago']) && $item['forma_de_pago'] != "") {?>
                    <p><b>Forma de Pago</b>: <?php echo $item['forma_de_pago']; ?></p>
                    <?php } ?>
                    <?php if (isset($item['comentario']) && $item['comentario'] != "") {?>
                    <p><b>Comentario</b>: <?php echo $item['comentario']; ?></p>
                    <?php } ?>
                </td>
                <td colspan="" rowspan="" headers="" style="width: 50%;vertical-align: top;">
                    <h3>Informaci&oacute;n sobre el vendedor</h3>
                    <?php if (isset($item['usuario_email']) && $item['usuario_email'] != "") {?>
                    <p><b>Email</b>: <?php echo $item['usuario_email']; ?></p>
                    <?php } ?>
                    <?php if (isset($item['usuario_domicilio']) && $item['usuario_domicilio'] != "") {?>
                    <p><b>Domicilio</b>: <?php echo $item['usuario_domicilio']; ?></p>
                    <?php } ?>
                    <?php if (isset($item['usuario_tel_fijo']) && $item['usuario_tel_fijo'] != "") {?>
                    <p><b>Tel&eacute;fono Fijo</b>: <?php echo $item['usuario_tel_fijo']; ?></p>
                    <?php } ?>
                    <?php if (isset($item['usuario_tel_movil']) && $item['usuario_tel_movil'] != "") {?>
                    <p><b>Tel&eacute;fono M&oacute;vil</b>: <?php echo $item['usuario_tel_movil']; ?></p>
                    <?php } ?>
                </td>
            </tr>
        </tbody>
    </table>
    <hr>
    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
        <tbody>
            <tr>
                <td colspan="" rowspan="" headers="" style="width: 33%;">
                    <?php if (isset($item['fecha_de_carga']) && $item['fecha_de_carga'] != "") {?>
                    <p><b>Fecha de carga:</b> <?php echo date('d/m/Y',strtotime($item['fecha_de_carga']));?></p>
                    <?php } ?>
                </td>
                <td colspan="" rowspan="" headers="" style="width: 33%;">
                    <?php if (isset($item['valido_hasta']) && $item['valido_hasta'] != "") {?>
                    <p><b>V&aacute;lido hasta:</b> <?php echo date('d/m/Y',strtotime($item['valido_hasta']));?></p>
                    <?php } ?>
                </td>
                <td colspan="" rowspan="" headers="" style="width: 33%;">
                    <?php if (isset($item['estado']) && $item['estado'] != "") {?>
                    <p><b>Estado de la publicaci&oacute;n:</b> <?php echo $item['estado']; ?></p>
                    <?php } ?>
                </td>
            </tr>
        </tbody>
    </table>
  </div>
</div>
