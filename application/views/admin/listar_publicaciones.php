<div class="row">
    <div class="large-12 columns">
        <h1>Listado de publicaciones</h1>
        <dl class="sub-nav">
          <dt>Filtrar:</dt>
          <dd <?php if($filtro=="main"){echo 'class="active"';}?>><a href="<?php echo base_url('admin/listado_publicaciones/main'); ?>">Todos</a></dd>
          <dd <?php if($filtro=="pendiente"){echo 'class="active"';}?>><a href="<?php echo base_url('admin/listado_publicaciones/pendiente'); ?>">Pendientes</a></dd>
          <dd <?php if($filtro=="procesando_pago"){echo 'class="active"';}?>><a href="<?php echo base_url('admin/listado_publicaciones/procesando_pago'); ?>">Procesando Pago</a></dd>
          <dd <?php if($filtro=="activo"){echo 'class="active"';}?>><a href="<?php echo base_url('admin/listado_publicaciones/activo'); ?>">Activos</a></dd>
          <dd <?php if($filtro=="inactivo"){echo 'class="active"';}?>><a href="<?php echo base_url('admin/listado_publicaciones/inactivo'); ?>">Inactivos</a></dd>
          <dd <?php if($filtro=="vencido"){echo 'class="active"';}?>><a href="<?php echo base_url('admin/listado_publicaciones/vencido'); ?>">Vencidos</a></dd>
        </dl>
        <table>
            <tr>
                <th>ID</th>
                <th>Titulo</th>
                <th>Usuario</th>
                <th>Fecha carga</th>
                <th>Estado</th>
                <th>Valido hasta</th>
                <th>Acciones</th>
            </tr>
            <?php foreach($publicaciones as $publicacion):?>
            <tr>
                <td><?= $publicacion->id?></td>
                <td><?= $publicacion->titulo?></td>
                <td><?= $publicacion->email?></td>
                <td><?= $publicacion->fecha_de_carga?></td>
                <td><?= $publicacion->estado?></td>
                <td><?= $publicacion->valido_hasta?></td>
                <td>
                    <a href="<?= base_url('admin/editar_publicacion/'.$publicacion->id) ?>">Estado</a> |
                    <a href="<?= base_url('admin/editar_publicacion_validez/'.$publicacion->id) ?>">Vencimiento</a> |
                    <a href="<?= base_url('admin/eliminar_publicacion/'.$publicacion->id) ?>">Eliminar</a>
                </td>
            </tr>
            <?php endforeach;?>
        </table>
        <p><?php echo $links; ?></p>
    </div>
</div>


