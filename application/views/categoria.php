<div class="content-home">
  <div id="container">
    <h1><?php echo $categoria_nombre; ?> - <?php echo count($categoria_items) ?> Item<?php if (count($categoria_items)>1) echo "s"; ?></h1>
    <?php
    $q = 0;
    foreach($categoria_items as $item){
      $q += 1;
      if ($q == 1) {echo "<div class='item-row'>";} ?>
      <a class="item" href="<?php echo base_url('item/'.$item->id_item); ?>">
        <div class="img" id="img<?php echo $item->id_item ?>"></div>
        <style>#img<?php echo $item->id_item ?>{background-image:url(<?php echo base_url('assets/uploads/'.$item->nombre_imagen) ?>);}</style>
        <h2><?php echo $item->titulo ?></h2>
        <!--p>Id de la publicaci&oacute;n: <?php echo $item->id ?></p-->
      </a>
    <?php
    if ($q == 4) {echo "</div>";$q = 0;};
    }
    if ($q != 0) {echo "</div>";};
    ?>
  </div>
</div>
