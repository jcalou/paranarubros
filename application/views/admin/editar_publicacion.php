<div class="row">
  <div class="large-12 columns">
    <h1>Editar publicacion - Cambiar estado</h1>
    <?= validation_errors(); ?>
    <form action="<?php echo base_url('admin/editar_publicacion'); ?>" method="POST">
      <div class="row">
        <div class="large-2 columns">
          <label for="estado">Estado:</label>
        </div>
        <div class="large-8 columns">
          <select name="estado" id="estado">
            <option <?php if($publicacion['estado']=="pendiente"){echo "selected";}?> value="pendiente">Pendiente</option>
            <option <?php if($publicacion['estado']=="procesando_pago"){echo "selected";}?> value="procesando_pago">Procesando Pago</option>
            <option <?php if($publicacion['estado']=="activo"){echo "selected";}?> value="activo">Activo</option>
            <option <?php if($publicacion['estado']=="inactivo"){echo "selected";}?> value="inactivo">Inactivo</option>
            <option <?php if($publicacion['estado']=="vencido"){echo "selected";}?> value="vencido">Vencido</option>
          </select>
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      
      <div class="row">
        <div class="large-10 columns">
          <input type="hidden" name="id" value="<?=$publicacion['id']?>" />
          <input type="hidden" name="post" value="1" />
          <input type="submit" class="button right" value="Guardar cambios"/>
        </div>
      </div>
    </form>
  </div>
</div>
