<?php
  function categorias() {

        $CI =& get_instance();
        $CI->load->model('categorias_model');
        $var = $CI->categorias_model->get_categorias();

        $categorias = array();

          for ($i = 0; $i < count($var); $i++) {
            $push = array($var[$i]->nombre." (".$var[$i]->cantidad.")",$var[$i]->url,$var[$i]->id);
            array_push($categorias, $push);
          }

        return $categorias;
    }

    function categoria_name($cat) {
      $categorias = categorias();
      for ($row = 0; $row < count($categorias); $row++){
        if ($categorias[$row][1]==$cat || $categorias[$row][2]==$cat) {
            return $categorias[$row][0];
        }
      }
      return "";
    }

    function categorias_nn() {

        $CI =& get_instance();
        $CI->load->model('categorias_model');
        $var = $CI->categorias_model->get_categorias();

        $categorias = array();

          for ($i = 0; $i < count($var); $i++) {
            $push = array($var[$i]->nombre,$var[$i]->url,$var[$i]->id);
            array_push($categorias, $push);
          }

        return $categorias;
    }

    function categoria_name_nn($cat) {
      $categorias = categorias_nn();
      for ($row = 0; $row < count($categorias); $row++){
        if ($categorias[$row][1]==$cat || $categorias[$row][2]==$cat) {
            return $categorias[$row][0];
        }
      }
      return "";
    }

    function categoria_select($req="") {
      $categorias = categorias();
      $required="";
      if($req=="required"){$required='class="required"';}
      $select = '<select name="categoria" id="categoria" '.$required.'><option value="">Seleccione una categor&iacute;a</option>';
      for ($row = 0; $row < count($categorias); $row++){
        $select = $select . '<option value="'.$categorias[$row][2].'">'.$categorias[$row][0].'</option>';
      }
      $select = $select . '</select>';
      return $select;


    }
