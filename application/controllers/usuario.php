<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Usuario extends CI_Controller
{
    public function __construct()
    {
        //Cargamos El Constructor
        parent::__construct();
        //Cargamos El modelo usuarios
        $this->load->model('usuarios_model');
        $this->load->model('publicacion_model');
        //Cargamos la libreria Form_validation para nuestro formulario
        $this->load->library('form_validation');
        $this->load->helper('usuarios_helper');
        $this->load->helper('mercadopago');
    }

    public function index()
    {
        //
    }

    public function login()
    {
        //Si Existe Post y es igual a uno
        if($this->input->post('post') && $this->input->post('post')==1)
        {
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');

            $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');
            $this->form_validation->set_message('valid_email','El Campo <b>%s</b> debe contener una direccion de mail');

            if ($this->form_validation->run() == TRUE)
            {
                $this->usuarios_model->email = $this->input->post('email');
                $this->usuarios_model->password = $this->input->post('password');

                $result = $this->usuarios_model->login();

                if($result) {
                    $sess_array = array();
                    foreach($result as $row) {
                        $sess_array = array(
                            'id' => $row->id,
                            'email' => $row->email
                        );
                        $this->session->set_userdata('logged_in', $sess_array);
                    }
                    redirect('usuario/micuenta');
                    return TRUE;
                } else {
                    redirect('usuario/login');
                    return false;
                }
            }
        }

        $data['title'] = 'Paranarubros.com.ar - Login - Ingreso al sistema';
        $data['description'] = 'Paranarubros.com.ar - Login - Ingreso al sistema';
        if($this->session->userdata('logged_in')) {
          redirect('usuario/micuenta');
        }else{
          $data['email'] = "";
        }

        $this->load->view('includes/header', $data);
        $this->load->view('usuario/login');
        $this->load->view('includes/footer');
        $this->load->view('includes/categories_nav');
    }

    public function registro()
    {
        //Si Existe Post y es igual a uno
        if($this->input->post('post') && $this->input->post('post')==1)
        {
            $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|email|xss_clean');

            $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');

            if ($this->form_validation->run() == TRUE)
            {
                $this->usuarios_model->nombre = $this->input->post('nombre');
                $this->usuarios_model->email = $this->input->post('email');
                $this->usuarios_model->password = $this->input->post('password');
                $this->usuarios_model->tel_fijo = $this->input->post('tel_fijo');
                $this->usuarios_model->tel_movil = $this->input->post('tel_movil');
                $this->usuarios_model->domicilio = $this->input->post('domicilio');
                $this->usuarios_model->localidad = $this->input->post('localidad');
                $this->usuarios_model->cod_post = $this->input->post('cod_post');
                $this->usuarios_model->provincia = $this->input->post('provincia');

                $this->usuarios_model->agregar_usuario();

                $result = $this->usuarios_model->login();

                if($result) {
                    $sess_array = array();
                    foreach($result as $row) {
                        $sess_array = array(
                            'id' => $row->id,
                            'email' => $row->email
                        );
                        $this->session->set_userdata('logged_in', $sess_array);
                    }
                    redirect('usuario/micuenta');
                    return TRUE;
                }
            }
        }

        $data['title'] = 'Paranarubros.com.ar - Resgitrar una nueva cuenta';
        $data['description'] = 'Paranarubros.com.ar - Resgitrar una nueva cuenta';
        if($this->session->userdata('logged_in')) {
            redirect('usuario/micuenta');
        }else{
            $data['email'] = "";
            $this->load->view('includes/header', $data);
            $this->load->view('usuario/registro');
            $this->load->view('includes/footer');
            $this->load->view('includes/categories_nav');
        }
        return false;
    }

    function logout() {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('main');
    }

    public function micuenta()
    {
        $data['title'] = 'Paranarubros.com.ar - Mi cuenta';
        $data['description'] = 'Paranarubros.com.ar - Mi cuenta';
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['email'] = $session_data['email'];
            $this->load->view('includes/header', $data);
            $this->load->view('usuario/micuenta');
            $this->load->view('includes/footer');
            $this->load->view('includes/categories_nav');
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function mispublicaciones()
    {
        $data['title'] = 'Paranarubros.com.ar - Mis publicaciones';
        $data['description'] = 'Paranarubros.com.ar - Mis publicaciones';
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['email'] = $session_data['email'];
            $data['publicaciones'] = $this->publicacion_model->get_publicaciones_user($session_data['id']);
            $this->load->view('includes/header', $data);
            $this->load->view('usuario/mispublicaciones', $data);
            $this->load->view('includes/footer');
            $this->load->view('includes/categories_nav');
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function editar()
    {
        $data['title'] = 'Paranarubros.com.ar - Editar datos personales';
        $data['description'] = 'Paranarubros.com.ar - Editar datos personales';

        if($this->session->userdata('logged_in')) {
            //Si existe el post para editar
            $session_data = $this->session->userdata('logged_in');
            if($this->input->post('post') && $this->input->post('post')==1)
            {
                $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|xss_clean');
                $this->form_validation->set_rules('email', 'Email', 'required|trim|email|xss_clean');

                $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');

                if ($this->form_validation->run() == TRUE)
                {
                    $this->usuarios_model->nombre = $this->input->post('nombre');
                    $this->usuarios_model->email = $this->input->post('email');
                    $this->usuarios_model->password = $this->input->post('password');
                    $this->usuarios_model->tel_fijo = $this->input->post('tel_fijo');
                    $this->usuarios_model->tel_movil = $this->input->post('tel_movil');
                    $this->usuarios_model->domicilio = $this->input->post('domicilio');
                    $this->usuarios_model->localidad = $this->input->post('localidad');
                    $this->usuarios_model->cod_post = $this->input->post('cod_post');
                    $this->usuarios_model->provincia = $this->input->post('provincia');

                    if ($this->input->post('password') != "") {
                      $this->usuarios_model->actualizar_usuario($session_data['id']);
                    }else {
                      $this->usuarios_model->actualizar_usuario_sin_pass($session_data['id']);
                    }
                    redirect('usuario/micuenta');
                }
            }

            $data['email'] = $session_data['email'];
            $data['usuario_logueado'] = $this->usuarios_model->get_usuario($session_data['id']);
            $this->load->view('includes/header', $data);
            $this->load->view('usuario/editar', $data);
            $this->load->view('includes/footer');
            $this->load->view('includes/categories_nav');
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function publicar()
    {
        //Si Existe Post y es igual a uno
        if($this->input->post('post') && $this->input->post('post')==1)
        {
            $this->form_validation->set_rules('titulo', 'Titulo', 'required|trim|xss_clean');

            $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');

            if ($this->form_validation->run() == TRUE)
            {

                $this->publicacion_model->id_categoria = $this->input->post('categoria');
                $this->publicacion_model->titulo = $this->input->post('titulo');
                $this->publicacion_model->descripcion = $this->input->post('descripcion');
                $this->publicacion_model->precio = $this->input->post('precio');
                $this->publicacion_model->forma_de_pago = $this->input->post('formapago');
                $this->publicacion_model->comentario = $this->input->post('comentario');
                $this->publicacion_model->cantidad_dias = $this->input->post('publicarpor');
                $this->publicacion_model->valido_hasta = $this->input->post('valido_hasta');
                $this->publicacion_model->estado = "pendiente";
                $this->publicacion_model->valor = "20";

                if($this->session->userdata('logged_in')) {
                    $session_data = $this->session->userdata('logged_in');
                    $usuario_logueado = $this->usuarios_model->get_usuario($session_data['id']);
                    $this->publicacion_model->id_usuario = $session_data['id'];
                    if ($this->input->post('pubmail')){
                        $this->publicacion_model->usuario_email = $usuario_logueado['email'];
                    }
                    if ($this->input->post('pubdomicilio')){
                        $this->publicacion_model->usuario_domicilio = $usuario_logueado['domicilio'];
                    }
                    if ($this->input->post('pubtel_fijo')){
                        $this->publicacion_model->usuario_tel_fijo = $usuario_logueado['tel_fijo'];
                    }
                    if ($this->input->post('pubtel_movil')){
                        $this->publicacion_model->usuario_tel_movil = $usuario_logueado['tel_movil'];
                    }
                }

                //agregar nuevo aviso

                $id_nuevo = $this->publicacion_model->agregar_publicacion();
                redirect('usuario/publicar_imagenes/'.$id_nuevo);

            }
        }

        $data['title'] = 'Paranarubros.com.ar - Publicar un aviso';
        $data['description'] = 'Paranarubros.com.ar - Publicar un aviso';
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['email'] = $session_data['email'];
            $data['usuario_logueado'] = $this->usuarios_model->get_usuario($session_data['id']);
            $this->load->view('includes/header', $data);
            $this->load->view('usuario/publicar');
            $this->load->view('includes/footer');
            $this->load->view('includes/categories_nav');
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function editar_publicacion($pub)
    {
        //Si Existe Post y es igual a uno
        if($this->input->post('post') && $this->input->post('post')==1)
        {
            $this->form_validation->set_rules('titulo', 'Titulo', 'required|trim|xss_clean');

            $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');

            if ($this->form_validation->run() == TRUE)
            {
                $publi = $this->publicacion_model->get_publicacion($pub);

                $this->publicacion_model->id_categoria = $publi['id_categoria'];
                $this->publicacion_model->titulo = $this->input->post('titulo');
                $this->publicacion_model->descripcion = $this->input->post('descripcion');
                $this->publicacion_model->precio = $this->input->post('precio');
                $this->publicacion_model->forma_de_pago = $this->input->post('formapago');
                $this->publicacion_model->comentario = $this->input->post('comentario');
                $this->publicacion_model->cantidad_dias = $this->input->post('publicarpor');
                $this->publicacion_model->valido_hasta = $publi['valido_hasta'];
                $this->publicacion_model->estado = $publi['estado'];
                $this->publicacion_model->valor = $publi['valor'];

                if($this->session->userdata('logged_in')) {
                    $session_data = $this->session->userdata('logged_in');
                    $usuario_logueado = $this->usuarios_model->get_usuario($session_data['id']);
                    $this->publicacion_model->id_usuario = $session_data['id'];
                    if ($this->input->post('pubmail')){
                        $this->publicacion_model->usuario_email = $usuario_logueado['email'];
                    }
                    if ($this->input->post('pubdomicilio')){
                        $this->publicacion_model->usuario_domicilio = $usuario_logueado['domicilio'];
                    }
                    if ($this->input->post('pubtel_fijo')){
                        $this->publicacion_model->usuario_tel_fijo = $usuario_logueado['tel_fijo'];
                    }
                    if ($this->input->post('pubtel_movil')){
                        $this->publicacion_model->usuario_tel_movil = $usuario_logueado['tel_movil'];
                    }
                }

                //editar
                $editada = $this->publicacion_model->actualizar_publicacion($pub);
                redirect('usuario/mispublicaciones');

            }
        }

        $data['title'] = 'Paranarubros.com.ar - Editar Publicaci&oacute;n';
        $data['description'] = 'Paranarubros.com.ar - Editar Publicaci&oacute;n';
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['email'] = $session_data['email'];
            $data['usuario_logueado'] = $this->usuarios_model->get_usuario($session_data['id']);
            $data['id_publicacion'] = $pub;
            $data['publicacion'] = $this->publicacion_model->get_publicacion($pub);

            $this->load->view('includes/header', $data);
            $this->load->view('usuario/editar-publicacion');
            $this->load->view('includes/footer');
            $this->load->view('includes/categories_nav');
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function editar_publicacion_activo($pub)
    {
        //Si Existe Post y es igual a uno
        if($this->input->post('post') && $this->input->post('post')==1)
        {
            $this->form_validation->set_rules('titulo', 'Titulo', 'required|trim|xss_clean');

            $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');

            if ($this->form_validation->run() == TRUE)
            {

                $publi = $this->publicacion_model->get_publicacion($pub);

                $this->publicacion_model->id_categoria = $publi['id_categoria'];
                $this->publicacion_model->titulo = $this->input->post('titulo');
                $this->publicacion_model->descripcion = $this->input->post('descripcion');
                $this->publicacion_model->precio = $this->input->post('precio');
                $this->publicacion_model->forma_de_pago = $this->input->post('formapago');
                $this->publicacion_model->comentario = $this->input->post('comentario');
                $this->publicacion_model->cantidad_dias = $publi['cantidad_dias'];
                $this->publicacion_model->valido_hasta = $publi['valido_hasta'];
                $this->publicacion_model->estado = $publi['estado'];
                $this->publicacion_model->valor = $publi['precio'];


                if($this->session->userdata('logged_in')) {
                    $session_data = $this->session->userdata('logged_in');
                    $usuario_logueado = $this->usuarios_model->get_usuario($session_data['id']);
                    $this->publicacion_model->id_usuario = $session_data['id'];
                    if ($this->input->post('pubmail')){
                        $this->publicacion_model->usuario_email = $usuario_logueado['email'];
                    }
                    if ($this->input->post('pubdomicilio')){
                        $this->publicacion_model->usuario_domicilio = $usuario_logueado['domicilio'];
                    }
                    if ($this->input->post('pubtel_fijo')){
                        $this->publicacion_model->usuario_tel_fijo = $usuario_logueado['tel_fijo'];
                    }
                    if ($this->input->post('pubtel_movil')){
                        $this->publicacion_model->usuario_tel_movil = $usuario_logueado['tel_movil'];
                    }
                }

                //editar
                $editada = $this->publicacion_model->actualizar_publicacion($pub);
                redirect('usuario/mispublicaciones');

            }
        }

        $data['title'] = 'Paranarubros.com.ar - Editar Publicaci&oacute;n';
        $data['description'] = 'Paranarubros.com.ar - Editar Publicaci&oacute;n';
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['email'] = $session_data['email'];
            $data['usuario_logueado'] = $this->usuarios_model->get_usuario($session_data['id']);
            $data['id_publicacion'] = $pub;
            $data['publicacion'] = $this->publicacion_model->get_publicacion($pub);

            $this->load->view('includes/header', $data);
            $this->load->view('usuario/editar-publicacion-activo');
            $this->load->view('includes/footer');
            $this->load->view('includes/categories_nav');
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function ocultar_publicacion($pub)
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['id_usuario'] = $session_data['id'];

            //chequear que la publicacion sea del usuario
            $publicacion = $this->publicacion_model->get_publicacion($pub);

            if ($publicacion['id_usuario'] == $session_data['id']) {
                $this->publicacion_model->cambiar_estado($pub,"inactivo");
                redirect('usuario/mispublicaciones');               
            }else{
                redirect('usuario/login');
            }
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function mostrar_publicacion($pub)
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['id_usuario'] = $session_data['id'];

            //chequear que la publicacion sea del usuario
            $publicacion = $this->publicacion_model->get_publicacion($pub);

            if ($publicacion['id_usuario'] == $session_data['id']) {
                $this->publicacion_model->cambiar_estado($pub,"activo");
                redirect('usuario/mispublicaciones');               
            }else{
                redirect('usuario/login');
            }
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function activar_publicacion($pub)
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['id_usuario'] = $session_data['id'];

            //chequear que la publicacion sea del usuario
            $publicacion = $this->publicacion_model->get_publicacion($pub);

            if ($publicacion['id_usuario'] == $session_data['id']) {
                $this->publicacion_model->activar($pub);
                redirect('usuario/mispublicaciones');               
            }else{
                redirect('usuario/login');
            }
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function procesando_pago($pub)
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['id_usuario'] = $session_data['id'];

            //chequear que la publicacion sea del usuario
            $publicacion = $this->publicacion_model->get_publicacion($pub);

            if ($publicacion['id_usuario'] == $session_data['id']) {
                $this->publicacion_model->cambiar_estado($pub,"procesando_pago");
                redirect('usuario/mispublicaciones');
            }else{
                redirect('usuario/login');
            }
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function publicar_imagenes($pub)
    {
        $data['title'] = 'Paranarubros.com.ar - Publicar im&aacute;genes';
        $data['description'] = 'Paranarubros.com.ar - Publicar im&aacute;genes';
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['email'] = $session_data['email'];
            $data['usuario_logueado'] = $this->usuarios_model->get_usuario($session_data['id']);
            $data['id_usuario'] = $session_data['id'];
            $data['id_publicacion'] = $pub;
            $data['cant'] = $this->publicacion_model->contar_imagenes($pub);

            //chequear que la publicacion sea del usuario
            $publicacion = $this->publicacion_model->get_publicacion($pub);

            if ($publicacion['id_usuario'] == $session_data['id']) {
                $data['imagenes'] = $this->publicacion_model->get_imagenes($pub);
                $this->load->view('includes/header', $data);
                $this->load->view('usuario/publicar-imagenes', $data);
                $this->load->view('includes/footer');
                $this->load->view('includes/categories_nav');
            }else {
                redirect('usuario/micuenta');
            }
        }else{
            redirect('usuario/login');
        }
        return false;
    }

    public function ajaximage()
    {
        //$path = 'C:\gitprojects\paranarubros\assets\uploads';
        $path = '/home/jcalou/public_html/assets/uploads';

        $valid_formats = array("jpg", "png", "gif","jpeg","PNG","JPG","JPEG","GIF");
        if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
            {
                $name = $_FILES['photoimg']['name'];
                $size = $_FILES['photoimg']['size'];

                if(strlen($name))
                    {
                        $ext = getExtension($name);
                        if(in_array($ext,$valid_formats))
                        {
                        if($size<(2200*2200))
                            {
                                $actual_image_name = $_POST['id_usuario']."-".$_POST['id_publicacion']."-".time().".".$ext;
                                $tmp = $_FILES['photoimg']['tmp_name'];
                                $cant = $this->publicacion_model->contar_imagenes($_POST['id_publicacion']);

                                if ($cant < 5) {
                                    if(move_uploaded_file($tmp, $path."/".$actual_image_name))
                                        {
                                          $main = "N";
                                          if ($cant == 0) {
                                            $main = "S";
                                            $result = "<div class='wrappreview'><img src='".base_url('assets/uploads')."/".$actual_image_name."'  class='preview'><p>Imagen principal</p></div>";
                                          }else{
                                            $result = "<div class='wrappreview'><img src='".base_url('assets/uploads')."/".$actual_image_name."'  class='preview'><a href='".base_url('usuario/eliminar_imagen/'.$actual_image_name)."'>Eliminar imagen</a><br><a href='".base_url('usuario/setear_imagen_principal/'.$_POST['id_publicacion'].'/'.$actual_image_name)."'>Establecer como principal</a></div>";
                                          }
                                          $this->publicacion_model->agregar_imagen($_POST['id_publicacion'],$actual_image_name,$main);

                                          echo $result;
                                        } else
                                            echo "Problemas de permisos en la carpeta uploads";
                                    } else
                                        echo "M&aacute;ximo 5 imagenes por publicaci&oacute;n";
                            } else
                                echo "La imagen es mas grande que 2MP";
                            }
                            else
                                echo "Formato invalido. Solo JPG, PNG o GIF";
                    } else
                    echo "Por favor seleccione una imagen";
                exit;
            }
    }

    public function eliminar_imagen($nombre,$id)
    {
        //$filefullname = 'C:\gitprojects\paranarubros\assets\uploads\\'. $nombre;
        $filefullname = '/home/jcalou/public_html/assets/uploads/'. $nombre;
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $imagen = $this->publicacion_model->get_imagen($nombre,$id);

            if ($imagen['id_usuario'] == $session_data['id']) {

                //eliminar imagen del disco
                if (file_exists($filefullname)) {
                    if ($nombre != "paranarubros.jpg") {
                        unlink($filefullname);
                    }                    
                }
                //eliminar imagen de MySQL
                $publicacion = $this->publicacion_model->eliminar_imagen($nombre,$id);

                redirect('usuario/publicar_imagenes/'.$imagen['id_publicacion']);
            } else{
                redirect('usuario/micuenta');
            }
        } else {
            redirect('usuario/login');
        }

    }

    public function setear_imagen_principal($id,$nombre)
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $imagen = $this->publicacion_model->get_imagen($nombre,$id);
            if ($imagen['id_usuario'] == $session_data['id']) {
                $publicacion = $this->publicacion_model->setear_imagen_principal($id,$nombre);
                redirect('usuario/publicar_imagenes/'.$imagen['id_publicacion']);
            } else{
                redirect('usuario/micuenta');
            }
        } else {
            redirect('usuario/login');
        }
    }

    public function eliminar_publicacion($id=0)
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $publicacion = $this->publicacion_model->get_publicacion($id);

            if ($publicacion['id_usuario'] == $session_data['id']) {
                $publicacion = $this->publicacion_model->eliminar_publicacion($id);
                redirect('usuario/micuenta/');
            } else{
                redirect('usuario/micuenta');
            }
        } else {
            redirect('usuario/login');
        }
    }

    public function pagar_publicacion($pub)
    {
        $data['title'] = 'Paranarubros.com.ar - Pagar publicacion';
        $data['description'] = 'Paranarubros.com.ar - Pagar publicacion';
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['email'] = $session_data['email'];
            $data['usuario_logueado'] = $this->usuarios_model->get_usuario($session_data['id']);
            $data['id_usuario'] = $session_data['id'];

            //chequear que la publicacion sea del usuario
            $publicacion = $this->publicacion_model->get_publicacion($pub);
            $data['publicacion'] = $publicacion;

            if ($publicacion['id_usuario'] == $session_data['id']) {
                $this->load->view('includes/header', $data);
                $this->load->view('usuario/pagar-publicacion');
                $this->load->view('includes/footer');
                $this->load->view('includes/categories_nav');
            }else {
                redirect('usuario/micuenta');
            }
        }else{
            redirect('usuario/login');
        }
        return false;
    }

}
