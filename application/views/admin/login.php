<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/foundation.css') ?>" />
    <script src="<?php echo base_url('assets/js/vendor/modernizr-2.6.2.min.js') ?>"></script>
  </head>
  <body>
    <div class="row">
      <div class="small-2 large-4 columns">&nbsp;</div>
      <div class="small-4 large-4 columns">
        <div class="large-12 columns">
          <h1>Admin Login</h1>
          <div class="form_errors">
            <?php echo validation_errors(); ?>
          </div>
        </div>
        <div class="large-12 columns">
          <form action="<?php echo base_url('admin/login') ?>" method="POST">
            <div class="row">
              <div class="small-3 columns">
                <label for="user" class="right">User:</label>
              </div>
              <div class="small-9 columns">
                <input type="text" id="user" name="user" placeholder="admin user">
              </div>
            </div>
            <div class="row">
              <div class="small-3 columns">
                <label for="password" class="right">Password:</label>
              </div>
              <div class="small-9 columns">
                <input type="password" id="password" name="password" placeholder="admin password">
              </div>
            </div>
          </div>
        <div class="large-12 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" class="button right" value="Login"/>
        </div>
        </form>
      </div>
      <div class="small-6 large-4 columns">&nbsp;</div>
    </div>


