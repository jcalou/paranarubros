<?php
class publicacion_model extends CI_Model
{
    var $id_usuario = "";
    var $id_categoria = "";
    var $titulo = "";
    var $descripcion = "";
    var $precio = "";
    var $forma_de_pago = "";
    var $comentario = "";
    var $fecha_de_carga = "";
    var $cantidad_dias = "";
    var $valido_hasta = "";
    var $estado = ""; // pendiente, activo, inactivo, vencido, procesando_pago
    var $valor = "";
    var $usuario_email = "";
    var $usuario_domicilio = "";
    var $usuario_tel_fijo = "";
    var $usuario_tel_movil = "";

    public function __construct()
    {
        parent::__construct();
    }
    //vamos a cargar todos los usuarios
    public function get_publicaciones($filtro,$limit,$offset)
    {
        $query_nofilter = "SELECT u.email,p.* from publicacion p JOIN usuarios u ON u.id = p.id_usuario ";
        $query_filter = 'SELECT u.email,p.* from publicacion p JOIN usuarios u ON u.id = p.id_usuario WHERE estado = "' . $filtro . '" ';

        if (isset($limit) && $limit>0) {
            $query_nofilter .= " LIMIT ".$limit;
            $query_filter .= " LIMIT ".$limit;
        }

        if (isset($offset) && $offset>0) {
            $query_nofilter .= " OFFSET ".$offset;
            $query_filter .= " OFFSET ".$offset;
        }

        if (isset($filtro) && $filtro=="main") {
            $sql = $this->db->query($query_nofilter);
        }else {
            $sql = $this->db->query($query_filter);
        }

        return $sql->result();
    }
    public function record_count($filtro)
    {
        if (isset($filtro) && $filtro=="main") {
            $sql = $this->db->count_all('publicacion');
        }else {
            $sql = $this->db->where('estado',$filtro)->count_all_results('publicacion');
        }
        return $sql;
    }
    public function get_publicaciones_index()
    {
        $sql = $this->db->query('
            SELECT i.nombre as nombre_imagen ,p.* from publicacion p
            JOIN imagenes i ON i.id_publicacion = p.id
            WHERE i.principal = "S" AND p.estado = "activo" ORDER BY p.id DESC');
        return $sql->result();
    }
    public function get_publicaciones_user($id)
    {
        $sql = $this->db->query('SELECT i.*,p.* from publicacion p join imagenes i on i.id_publicacion = p.id where p.id_usuario="'.$id.'" and i.principal = "S"');
        return $sql->result();
    }
    public function busqueda_publicaciones($q)
    {
        $sql = $this->db->query('
            SELECT i.nombre as nombre_imagen ,p.id as id_item, p.*,c.* from publicacion p
            JOIN categoria c ON c.id = p.id_categoria
            JOIN imagenes i ON i.id_publicacion = p.id
            where i.principal = "S" and p.titulo LIKE "%'.$q.'%" ');
        return $sql->result();
    }
    public function busqueda_publicaciones_categoria($q,$url)
    {
        $sql = $this->db->query('
            SELECT i.nombre as nombre_imagen ,p.id as id_item, p.*,c.* from publicacion p
            JOIN categoria c ON c.id = p.id_categoria
            JOIN imagenes i ON i.id_publicacion = p.id
            where i.principal = "S" and c.id = "'.$url.'" and p.titulo LIKE "%'.$q.'%" ');
        return $sql->result();
    }
    public function get_publicaciones_categoria($url)
    {
        $sql = $this->db->query('
            SELECT i.nombre as nombre_imagen ,p.id as id_item, p.*,c.* from publicacion p
            JOIN categoria c ON c.id = p.id_categoria
            JOIN imagenes i ON i.id_publicacion = p.id
            where i.principal = "S" and p.estado="activo" and c.url = "'.$url.'"');
        return $sql->result();
    }

    //agregamos un usuario
    public function agregar_publicacion()
    {
        $this->db->insert('publicacion',array(
            'id_usuario'=> $this->id_usuario,
            'id_categoria' => $this->id_categoria,
            'titulo' => $this->titulo,
            'descripcion' => $this->descripcion,
            'precio' => $this->precio,
            'forma_de_pago' => $this->forma_de_pago,
            'comentario' => $this->comentario,
            'fecha_de_carga' => date("Y-m-d h:i:s"),
            'cantidad_dias' => $this->cantidad_dias,
            'valido_hasta' => $this->valido_hasta,
            'estado' => $this->estado,
            'valor' => $this->valor,
            'usuario_email' => $this->usuario_email,
            'usuario_domicilio' => $this->usuario_domicilio,
            'usuario_tel_fijo' => $this->usuario_tel_fijo,
            'usuario_tel_movil' => $this->usuario_tel_movil
        ));
        $insert_id = $this->db->insert_id();

        $this->agregar_imagen($insert_id,"paranarubros.jpg","S");

        return $insert_id;
    }
    //actualizamos los datos de un usuario por id
    public function actualizar_publicacion($id)
    {
        $this->db->where('id', $id);
        $this->db->update('publicacion',array(
            'id_usuario'=> $this->id_usuario,
            'id_categoria' => $this->id_categoria,
            'titulo' => $this->titulo,
            'descripcion' => $this->descripcion,
            'precio' => $this->precio,
            'forma_de_pago' => $this->forma_de_pago,
            'comentario' => $this->comentario,
            'cantidad_dias' => $this->cantidad_dias,
            'valido_hasta' => $this->valido_hasta,
            'estado' => $this->estado,
            'valor' => $this->valor,
            'usuario_email' => $this->usuario_email,
            'usuario_domicilio' => $this->usuario_domicilio,
            'usuario_tel_fijo' => $this->usuario_tel_fijo,
            'usuario_tel_movil' => $this->usuario_tel_movil
        ));
    }
    
    //cambiar estado de una publicacion
    public function cambiar_estado($id,$estado)
    {
        $this->db->where('id', $id);
        $this->db->update('publicacion',array(
            'estado' => $estado
        ));
    }
    public function cambiar_validez($id,$valido_hasta)
    {
        $this->db->where('id', $id);
        $this->db->update('publicacion',array(
            'valido_hasta' => $valido_hasta
        ));
    }

    //activar de una publicacion
    public function activar($id)
    {
        $publicacion = $this->get_publicacion($id);
        $valido_date = date('Y-m-d', strtotime("+" . $publicacion['cantidad_dias'] . " days"));

        $this->db->where('id', $id);
        $this->db->update('publicacion',array(
            'estado' => "activo",
            'valido_hasta' => $valido_date
        ));
    }

    //eliminamos un usuario por id
    public function eliminar_publicacion($id)
    {
        $this->db->delete('publicacion', array('id' => $id));
    }
    //Obtenemos los datos de un usuario por id
    public function get_publicacion($id)
    {
        $sql = $this->db->query('
            SELECT c.nombre as nombre_categoria, c.url as url_categoria, p.* from publicacion p
            JOIN categoria c ON c.id = p.id_categoria
            WHERE p.id = '.$id);
        if($sql->num_rows()==1)
        return $sql->row_array();
        else
        return false;
    }


    public function agregar_imagen($pub, $nombre, $main = "N")
    {
        $this->db->insert('imagenes',array(
            'id_publicacion'=> $pub,
            'nombre' => $nombre,
            'principal' => $main
        ));
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    //Obtenemos los datos de un usuario por id
    public function get_imagenes($id)
    {
        $this->db->select('*');
        $this->db->where('id_publicacion',$id);
        $this->db->order_by("id","desc");
        $this->db->from('imagenes');
        $sql=$this->db->get();

        if($sql->num_rows()>=1)
        return $sql->result_array();
        else
        return array();
    }
    public function contar_imagenes($id)
    {
        $sql = $this->db->get_where('imagenes',array('id_publicacion'=>$id));
        if($sql->num_rows()>=1)
        return $sql->num_rows();
        else
        return 0;
    }
    public function get_imagen($nombre,$id)
    {
        $sql = $this->db->query('SELECT i.*,p.id_usuario from imagenes i join publicacion p on i.id_publicacion = p.id where i.nombre="'.$nombre.'" and i.id_publicacion ="'.$id.'"');
        if($sql->num_rows()==1)
        return $sql->row_array();
        else
        return false;
    }
    public function eliminar_imagen($nombre,$id)
    {
        $this->db->delete('imagenes', array('nombre' => $nombre,'id_publicacion' => $id));
    }
    //actualizamos los datos de un usuario por id
    public function setear_imagen_principal($id,$nombre)
    {
        $this->db->where('id_publicacion', $id);
        $this->db->update('imagenes',array('principal'=> "N"));

        $this->db->flush_cache();

        $this->db->where('nombre', $nombre);
        $this->db->update('imagenes',array('principal'=> "S"));

    }

}
