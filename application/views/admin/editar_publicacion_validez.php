<div class="row">
  <div class="large-12 columns">
    <h1>Editar publicacion - Cambiar vencimiento</h1>
    <?= validation_errors(); ?>
    <form action="<?php echo base_url('admin/editar_publicacion_validez'); ?>" method="POST">
      <div class="row">
        <div class="large-2 columns">
          <label for="valido_hasta">Valido Hasta<br>(YYYY-MM-DD):</label>
        </div>
        <div class="large-8 columns">
          <input type="text" value="<?=$publicacion['valido_hasta'];?>" name="valido_hasta">
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      
      <div class="row">
        <div class="large-10 columns">
          <input type="hidden" name="id" value="<?=$publicacion['id']?>" />
          <input type="hidden" name="post" value="1" />
          <input type="submit" class="button right" value="Guardar cambios"/>
        </div>
      </div>
    </form>
  </div>
</div>
