<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Admin extends CI_Controller
{
    public function __construct()
    {
        //Cargamos El Constructor
        parent::__construct();
        //Cargamos El modelo usuarios
        $this->load->model('usuarios_model');
        $this->load->model('publicacion_model');
        //Cargamos la libreria Form_validation para nuestro formulario
        $this->load->library('form_validation');
        $this->load->library("pagination");
    }

    public function login()
    {
        //Si Existe Post y es igual a uno
        if($this->input->post('post') && $this->input->post('post')==1)
        {
            $this->form_validation->set_rules('user', 'User', 'required|trim|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');

            $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');

            if ($this->form_validation->run() == TRUE)
            {
                $user = $this->input->post('user');
                $password = $this->input->post('password');

                if ($user=="admin" && $password=="asdasd"){
                    $sess_array = array();
                    $sess_array = array(
                        'id' => 1,
                        'admin' => true
                    );
                    $this->session->set_userdata('logged_in_admin', $sess_array);
                    redirect('admin/dashboard');
                } else {
                    redirect('admin/login');
                    return false;
                }
            }
        }

        $data['title'] = 'Paranarubros.com.ar - Admin';
        $data['description'] = 'Paranarubros.com.ar';

        $this->load->view('admin/login');
        $this->load->view('admin/footer');
    }

     function logout() {
        $this->session->unset_userdata('logged_in_admin');
        session_destroy();
        redirect('admin/login');
    }

    public function dashboard(){
        $data['title'] = 'Paranarubros.com.ar - Panel de control';
        $data['description'] = 'Paranarubros.com.ar';

        if($this->session->userdata('logged_in_admin')) {
          $session_data = $this->session->userdata('logged_in_admin');
        }else{
          redirect('admin/login');
        }
        $this->load->view('admin/header', $data);
        $this->load->view('admin/dashboard');
        $this->load->view('admin/footer');
    }

    public function procesar_vencimientos(){
        $data['title'] = 'Paranarubros.com.ar - Procesar vencimientos';
        $data['description'] = 'Paranarubros.com.ar';

        if($this->session->userdata('logged_in_admin')) {
          $session_data = $this->session->userdata('logged_in_admin');
        }else{
          redirect('admin/login');
        }

        $publicaciones = $this->publicacion_model->get_publicaciones("",0,0);
        $data['publicaciones'] = $publicaciones;
        $data['model_publicaciones'] = $this->publicacion_model;

        $this->load->view('admin/header', $data);
        $this->load->view('admin/procesar_vencimientos', $data);
        $this->load->view('admin/footer');
    }

    public function listado_usuarios(){
        $data['title'] = 'Paranarubros.com.ar - Panel de control';
        $data['description'] = 'Paranarubros.com.ar';

        if($this->session->userdata('logged_in_admin')) {
          $session_data = $this->session->userdata('logged_in_admin');
        }else{
          redirect('admin/login');
        }

        $config = array();
        $config["base_url"] = base_url() . "admin/listado_usuarios";
        $config["total_rows"] = $this->usuarios_model->record_count();
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
 
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $usuarios = $this->usuarios_model->get_usuarios($config["per_page"], $page);
        
        $data["links"] = $this->pagination->create_links();
        $data['usuarios'] = $usuarios;

        $this->load->view('admin/header', $data);
        $this->load->view('admin/listar_usuario.php',$data);
        $this->load->view('admin/footer');

    }

    public function editar_usuario($id=0)
    {
        $data['title'] = 'Paranarubros.com.ar - Panel de control';
        $data['description'] = 'Paranarubros.com.ar';

        if($this->session->userdata('logged_in_admin')) {
          $session_data = $this->session->userdata('logged_in_admin');
        }else{
          redirect('admin/login');
        }

        $respuesta = $this->usuarios_model->get_usuario($id);

        if($respuesta==false)
            redirect('admin/dashboard');
        else
        {
            //Si existe el post para editar
            if($this->input->post('post') && $this->input->post('post')==1)
            {
                $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|xss_clean');
                $this->form_validation->set_rules('email', 'Email', 'required|trim|email|xss_clean');

                $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');

                if ($this->form_validation->run() == TRUE)
                {
                    $this->usuarios_model->nombre = $this->input->post('nombre');
                    $this->usuarios_model->email = $this->input->post('email');
                    $this->usuarios_model->password = $this->input->post('password');
                    $this->usuarios_model->tel_fijo = $this->input->post('tel_fijo');
                    $this->usuarios_model->tel_movil = $this->input->post('tel_movil');
                    $this->usuarios_model->domicilio = $this->input->post('domicilio');
                    $this->usuarios_model->localidad = $this->input->post('localidad');
                    $this->usuarios_model->cod_post = $this->input->post('cod_post');
                    $this->usuarios_model->provincia = $this->input->post('provincia');

                    $this->usuarios_model->actualizar_usuario();
                    //redireccionamos al controlador CRUD
                    redirect('admin/listado_usuarios');
                }
            }

            $data['usuario'] = $respuesta;

            $this->load->view('admin/header', $data);
            $this->load->view('admin/editar_usuario.php',$data);
            $this->load->view('admin/footer');
        }
    }

    public function agregar_usuario()
    {
        $data['title'] = 'Paranarubros.com.ar - Panel de control';
        $data['description'] = 'Paranarubros.com.ar';

        if($this->session->userdata('logged_in_admin')) {
          $session_data = $this->session->userdata('logged_in_admin');
        }else{
          redirect('admin/login');
        }

        if($this->input->post('post') && $this->input->post('post')==1)
        {
            $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|email|xss_clean');

            $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');

            if ($this->form_validation->run() == TRUE)
            {
                $this->usuarios_model->nombre = $this->input->post('nombre');
                $this->usuarios_model->email = $this->input->post('email');
                $this->usuarios_model->password = $this->input->post('password');
                $this->usuarios_model->tel_fijo = $this->input->post('tel_fijo');
                $this->usuarios_model->tel_movil = $this->input->post('tel_movil');
                $this->usuarios_model->domicilio = $this->input->post('domicilio');
                $this->usuarios_model->localidad = $this->input->post('localidad');
                $this->usuarios_model->cod_post = $this->input->post('cod_post');
                $this->usuarios_model->provincia = $this->input->post('provincia');

                $this->usuarios_model->agregar_usuario();
                //redireccionamos al controlador CRUD
                redirect('admin/listado_usuarios');
            }
        }

        $this->load->view('admin/header', $data);
        $this->load->view('admin/agregar_usuario.php');
        $this->load->view('admin/footer');

    }

    public function eliminar_usuario($id=0)
    {
        if($this->session->userdata('logged_in_admin')) {
          $session_data = $this->session->userdata('logged_in_admin');
        }else{
          show_404();
        }
        //verificamos si existe el id
        $respuesta = $this->usuarios_model->get_usuario($id);
        //si nos retorna FALSE mostramos la pag 404.
        if($respuesta==false)
            show_404();
        else
        {
            //si existe eliminamos el usuario
            $this->usuarios_model->eliminar_usuario($id);
            //redireccionamos al controlador CRUD
            redirect('admin/listado_usuarios');
        }
    }

    public function listado_publicaciones($filtro){
        $data['title'] = 'Paranarubros.com.ar - Listado de publicaciones';
        $data['description'] = 'Paranarubros.com.ar';

        if($this->session->userdata('logged_in_admin')) {
            $session_data = $this->session->userdata('logged_in_admin');

            $config = array();
            $config["base_url"] = base_url() . "admin/listado_publicaciones/".$filtro;
            $config["total_rows"] = $this->publicacion_model->record_count($filtro);
            $config["per_page"] = 5;
            $config["uri_segment"] = 4;
     
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

            $publicaciones = $this->publicacion_model->get_publicaciones($filtro,$config["per_page"], $page);
            
            $data["links"] = $this->pagination->create_links();
            $data['publicaciones'] = $publicaciones;
            $data['filtro'] = $filtro;

            $this->load->view('admin/header', $data);
            $this->load->view('admin/listar_publicaciones',$data);
            $this->load->view('admin/footer');

        }else{
          redirect('admin/login');
        }
    }

    public function editar_publicacion($id=0)
    {
        $data['title'] = 'Paranarubros.com.ar - Panel de control';
        $data['description'] = 'Paranarubros.com.ar';

        if($this->session->userdata('logged_in_admin')) {
          $session_data = $this->session->userdata('logged_in_admin');
        }else{
          redirect('admin/login');
        }
        
        //Si existe el post para editar
        if($this->input->post('post') && $this->input->post('post')==1)
        {
            $this->publicacion_model->cambiar_estado($this->input->post('id'),$this->input->post('estado'));

            //redireccionamos al controlador CRUD
            redirect('admin/listado_publicaciones/main');
        }else {
            $respuesta = $this->publicacion_model->get_publicacion($id);
            if($respuesta==false) redirect('admin/dashboard');
        }

        $data['publicacion'] = $respuesta;

        $this->load->view('admin/header', $data);
        $this->load->view('admin/editar_publicacion',$data);
        $this->load->view('admin/footer');
    }

    public function editar_publicacion_validez($id=0)
    {
        $data['title'] = 'Paranarubros.com.ar - Panel de control';
        $data['description'] = 'Paranarubros.com.ar';

        if($this->session->userdata('logged_in_admin')) {
          $session_data = $this->session->userdata('logged_in_admin');
        }else{
          redirect('admin/login');
        }
        
        //Si existe el post para editar
        if($this->input->post('post') && $this->input->post('post')==1)
        {
            
            $this->publicacion_model->cambiar_validez($this->input->post('id'),$this->input->post('valido_hasta'));

            //redireccionamos al controlador CRUD
            redirect('admin/listado_publicaciones/main');
        }else {
            $respuesta = $this->publicacion_model->get_publicacion($id);
            if($respuesta==false) redirect('admin/dashboard');
        }

        $data['publicacion'] = $respuesta;

        $this->load->view('admin/header', $data);
        $this->load->view('admin/editar_publicacion_validez',$data);
        $this->load->view('admin/footer');
    }

}
