<nav class="sidebar jsc-sidebar" id="jsi-nav" data-sidebar-options="">
      <ul class="sidebar-list">
        <li><a href="<?php echo base_url(); ?>" <?=(!isset($categoria) ? "class='current'" : "") ?>>Inicio</a></li>
        <?php
            $categorias = categorias();
            for ($row = 0; $row < count($categorias); $row++){
                $link = 'categoria/' . $categorias[$row][1];
                $add = '<li><a href="' . base_url($link) . '"';
                if (isset($categoria) && $categoria==$categorias[$row][1]) {
                    $add = $add . "class='current'";
                }
                $add = $add . '>' . $categorias[$row][0] . '</a></li>';
                echo $add;
            }
        ?>

      </ul>
    </nav>
    <script src="<?php echo base_url('assets/js/vendor/jquery-1.10.2.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/main.js') ?>"></script>

</body>
</html>
