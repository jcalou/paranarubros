<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Main extends CI_Controller {

  public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('publicacion_model');
    }

	public function index(){
        $data['title'] = 'Paranarubros.com.ar - El sitio de compra y venta de Paran&aacute;';
        $data['description'] = 'Paranarubros.com.ar - El sitio de compra y venta de Paran&aacute;';

        if($this->session->userdata('logged_in')) {
          $session_data = $this->session->userdata('logged_in');
          $data['email'] = $session_data['email'];
        }else{
          $data['email'] = "";
        }

        $data['items'] = $this->publicacion_model->get_publicaciones_index();

        $this->load->view('includes/header', $data);
        $this->load->view('index', $data);
        $this->load->view('includes/footer');
        $this->load->view('includes/categories_nav');
	}

	public function nosotros()
	{
		$data['title'] = 'Paranarubros.com.ar - Nosotros';
		$data['description'] = 'Paranarubros.com.ar - Nosotros';
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['email'] = $session_data['email'];
    }else{
      $data['email'] = "";
    }

    $this->load->view('includes/header', $data);
    $this->load->view('nosotros');
    $this->load->view('includes/footer');
    $this->load->view('includes/categories_nav');
	}

  public function ayuda()
  {
    $data['title'] = 'Paranarubros.com.ar - Ayuda';
    $data['description'] = 'Paranarubros.com.ar - Ayuda';
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['email'] = $session_data['email'];
    }else{
      $data['email'] = "";
    }
    $this->load->view('includes/header', $data);
    $this->load->view('ayuda');
    $this->load->view('includes/footer');
    $this->load->view('includes/categories_nav');
  }

  public function buscar()
  {
    $data['title'] = 'Paranarubros.com.ar - Buscar publicaciones';
    $data['description'] = 'Paranarubros.com.ar - Buscar publicaciones';

    if($this->input->post('post') && $this->input->post('post')==1) {
        $this->form_validation->set_rules('buscar', 'buscar', 'required|trim|xss_clean');
        $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');

        if ($this->form_validation->run() == TRUE) {
            $buscar = $this->input->post('buscar');
            $cat = $this->input->post('categoria');

            if($this->session->userdata('logged_in')) {
              $session_data = $this->session->userdata('logged_in');
              $data['email'] = $session_data['email'];
            }else{
              $data['email'] = "";
            }

            //$data['categoria_nombre'] = categoria_name($cat);
            $data['categoria'] = $cat;
            $data['buscar'] = $buscar;
            if ($cat != ""){
              $data['items'] = $this->publicacion_model->busqueda_publicaciones_categoria($buscar,$cat);
            }else {
              $data['items'] = $this->publicacion_model->busqueda_publicaciones($buscar);
            }

            $this->load->view('includes/header', $data);
            $this->load->view('resultado_buscar', $data);
            $this->load->view('includes/footer');
            $this->load->view('includes/categories_nav');

            return false;
        }
    }

    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['email'] = $session_data['email'];
    }else{
      $data['email'] = "";
    }

    $this->load->view('includes/header', $data);
    $this->load->view('buscar');
    $this->load->view('includes/footer');
    $this->load->view('includes/categories_nav');
  }

  public function terminosycondiciones()
  {
    $data['title'] = 'Paranarubros.com.ar - T&eacute;rminos y Condiciones';
    $data['description'] = 'Paranarubros.com.ar - T&eacute;rminos y Condiciones';
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['email'] = $session_data['email'];
    }else{
      $data['email'] = "";
    }

    $this->load->view('includes/header', $data);
    $this->load->view('terminosycondiciones');
    $this->load->view('includes/footer');
    $this->load->view('includes/categories_nav');
  }

  public function contacto()
  {
    $data['title'] = 'Paranarubros.com.ar - Contacto';
    $data['description'] = 'Paranarubros.com.ar - Contacto';
    
    if($this->input->post('post') && $this->input->post('post')==1)
    {
        $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');
        $this->form_validation->set_rules('comentario', 'Comentario', 'required|trim|xss_clean');

        $this->form_validation->set_message('required','El Campo <b>%s</b> Es Obligatorio');
        $this->form_validation->set_message('valid_email','El Campo <b>%s</b> debe contener un email valido');

        if ($this->form_validation->run() == TRUE)
        {
            $nombre = $this->input->post('nombre');
            $email_ = $this->input->post('email');
            $comentario = $this->input->post('comentario');

            $mensaje  = "Nombre: " . $nombre . "\r\n";
            $mensaje .= "Email: " . $email_ . "\r\n";
            $mensaje .= "Comentario: " . $comentario;

            //enviar email
            $this->load->library('email');

            $this->email->initialize(array(
              'protocol' => 'sendmail',
              'mailpath' => '/usr/sbin/sendmail',
              'charset' => 'iso-8859-1'
            ));

            $this->email->from('contacto@paranarubros.com.ar', 'Paranarubros.com.ar');
            $this->email->to('municalou@hotmail.com');
            $this->email->subject('Contacto desde el sitio web');
            $this->email->message($mensaje);

            $this->email->send();

            //echo $this->email->print_debugger();
            redirect('main');

        }
    }

    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['email'] = $session_data['email'];
    }else{
      $data['email'] = "";
    }

    $this->load->view('includes/header', $data);
    $this->load->view('contacto');
    $this->load->view('includes/footer');
    $this->load->view('includes/categories_nav');
  }
}
