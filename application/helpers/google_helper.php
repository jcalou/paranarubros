<?php
/**
 * Helper to generate a Google Analytics trackingcode
 * @param  string $analytics_id The Google Analytics ID
 * @return string            Tracking Code for Google
*/
function analytics_trackingcode($analytics_id='')
{
    $analytics_id = ($analytics_id) ? $analytics_id : config_item('analytics_id');

    $trackingcode = "<script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '".$analytics_id."', 'auto');
        ga('send', 'pageview');

      </script>";

    return $trackingcode;
}