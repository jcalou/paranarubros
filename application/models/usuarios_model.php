<?php
class usuarios_model extends CI_Model
{
    var $nombre = "";
    var $email = "";
    var $password = "";
    var $tel_fijo = "";
    var $tel_movil = "";
    var $domicilio = "";
    var $localidad = "";
    var $cod_post = "";
    var $provincia = "";

    public function __construct()
    {
        parent::__construct();
    }
    //vamos a cargar todos los usuarios
    public function get_usuarios($limit, $offset)
    {
        $sql = $this->db->limit($limit,$offset)->get('usuarios');
        return $sql->result();
    }
    public function record_count()
    {
        $sql = $this->db->count_all('usuarios');
        return $sql;
    }
    //agregamos un usuario
    public function agregar_usuario()
    {
        $this->db->insert('usuarios',array(
            'nombre'=> $this->nombre,
            'email' => $this->email,
            'password' => MD5($this->password),
            'tel_fijo' => $this->tel_fijo,
            'tel_movil' => $this->tel_movil,
            'domicilio' => $this->domicilio,
            'localidad' => $this->localidad,
            'cod_post' => $this->cod_post,
            'provincia' => $this->provincia
        ));
    }
    //actualizamos los datos de un usuario por id
    public function actualizar_usuario($id)
    {
        $this->db->where('id', $id);
        $this->db->update('usuarios',array(
            'nombre'=> $this->nombre,
            'email' => $this->email,
            'password' => MD5($this->password),
            'tel_fijo' => $this->tel_fijo,
            'tel_movil' => $this->tel_movil,
            'domicilio' => $this->domicilio,
            'localidad' => $this->localidad,
            'cod_post' => $this->cod_post,
            'provincia' => $this->provincia
        ));
    }
    public function actualizar_usuario_sin_pass($id)
    {
        $this->db->where('id', $id);
        $this->db->update('usuarios',array(
            'nombre'=> $this->nombre,
            'email' => $this->email,
            'tel_fijo' => $this->tel_fijo,
            'tel_movil' => $this->tel_movil,
            'domicilio' => $this->domicilio,
            'localidad' => $this->localidad,
            'cod_post' => $this->cod_post,
            'provincia' => $this->provincia
        ));
    }
    //eliminamos un usuario por id
    public function eliminar_usuario($id)
    {
        $this->db->delete('usuarios', array('id' => $id));
    }
    //Obtenemos los datos de un usuario por id
    public function get_usuario($id)
    {
        $sql = $this->db->get_where('usuarios',array('id'=>$id));
        if($sql->num_rows()==1)
        return $sql->row_array();
        else
        return false;
    }

    public function login()
    {
        $this -> db -> select('id, email, password');
        $this -> db -> from('usuarios');
        $this -> db -> where('email', $this->email);
        $this -> db -> where('password', MD5($this->password));
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1){
            return $query->result();
        } else {
            return false;
        }
    }
}
