<div class="content-contacto">
  <div id="container">
    <h1>Contactate con Nosotros</h1>
    <div class="form_errors">
      <?php echo validation_errors(); ?>
    </div>
    <p>Hacenos llegar tu consulta y te responderemos a la brevedad</p>
    <form action="<?php echo base_url('contacto') ?>" method="POST">
      <label for="nombre">Nombre:</label>
      <input type="text" size="20" id="nombre" name="nombre"/>
      <div class="info">Ingrese su nombre completo</div>
      <br/>
      <label for="email">Email:</label>
      <input type="text" size="20" id="email" name="email"/>
      <div class="info">Ingrese su email</div>
      <br/>
      <label for="comentario">Comentario:</label>
      <textarea name="comentario" id="" cols="30" rows="10"></textarea>
      <div class="info">Ingrese su comentario o consulta</div>
      <br/>
      <input type="hidden" name="post" value="1" />
      <input type="submit" value="Enviar" />
      <br/>
    </form>
    <p>&nbsp;</p>
  </div>
</div>