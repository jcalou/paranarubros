<div class="contentb">
  <div id="container">
    <h1>Publicar un aviso - Imagenes</h1>
    <div class="form_errors">
      <?php echo validation_errors(); ?>
    </div>

    <h4>Im&aacute;genes de la publicaci&oacute;n</h4>
    <hr />
    <p><b>IMPORTANTE:</b> Una vez cargadas sus imagenes puede seleccionar una como principal y eliminal la imagen por defecto de paranarubros.</p>
    <p>La publicaci&oacute;n tiene un m&aacute;ximo de 5 imagenes.</p>
    <p>Haga click en "Seleccionar archivo" y seleccione la foto a cargar.</p>
    <p>Importante: s&oacute;lo puede subir fotos en formato gif, png o jpg, tama&ntilde;o m&aacute;ximo de 2MP.</p>
    <?php if ($cant < 5) {?>
    <form id="imageform" method="post" enctype="multipart/form-data" action='<?php echo base_url('usuario/ajaximage') ?>'>
      <div id='imageloadstatus' style='display:none'><img src="<?php echo base_url('assets/images') ?>/loader.gif" alt="Uploading...."/></div>
      <div id='imageloadbutton'>
        <input type="file" name="photoimg" id="photoimg" />
        <input type="hidden" name="id_usuario" value="<?php echo $id_usuario ?>" />
        <input type="hidden" name="id_publicacion" value="<?php echo $id_publicacion ?>" />
        <input type="hidden" name="post" value="1" />
      </div>
    </form>
    <?php } ?>
    <div id='preview'>
    <?php
    foreach ($imagenes as $key => $value) {
      echo "<div class='wrappreview'>";
      echo "<img src='".base_url('assets/uploads')."/".$value['nombre']."'  class='preview'>";
      if ($value['principal']=="N") {
        echo "<a href='".base_url('usuario/eliminar_imagen/'.$value['nombre']."/".$id_publicacion)."'>Eliminar imagen</a><br>";
        echo "<a href='".base_url('usuario/setear_imagen_principal/'.$id_publicacion.'/'.$value['nombre'])."'>Establecer como principal</a>";
      } else {
        echo "<p>Imagen principal</p>";
      }
      echo "</div>";
    }
    ?>
    </div>
    <p>Estas imagenes se pueden editar ingrensando en "MI CUENTA" en la opción "Mis publicaciones".</p>
    <div class="wrapbutton">
      <a href="<?php echo base_url('usuario/micuenta') ?>" class="button">Aceptar</a>
    </div>
  </div>
</div>
