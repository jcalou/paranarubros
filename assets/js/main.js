function errorPlacement(error, element){
    //element.before(error);
}
$(document).ready(function(){

  $('#jsi-nav').sidebar({
    trigger: '.jsc-sidebar-trigger',
    scrollbarDisplay: true,
    pullCb: function () {  },
    pushCb: function () {  }
  });

  $('input,textarea,select').focus(function(event) {
    event.preventDefault();
    $(this).next('.info').css({'opacity':1});
  });
  $('input,textarea,select').focusout(function(event) {
    event.preventDefault();
    $(this).next('.info').css({'opacity':0.5});
  });

  $("#publicar-form").validate({
    errorPlacement: errorPlacement
  });

  $("#wizard").steps({
    headerTag: "h2",
    bodyTag: "section",
    stepsOrientation: "vertical",
    enableAllSteps: true,
    transitionEffect: "fade",
    onStepChanging: function (event, currentIndex, newIndex) {
        $("#publicar-form").validate().settings.ignore = ":disabled,:hidden";
        return $("#publicar-form").valid();
    },
    onFinishing: function (event, currentIndex) {
        $("#publicar-form").validate().settings.ignore = ":disabled";
        return $("#publicar-form").valid();
    },
    onFinished: function (event, currentIndex) {
        $("#publicar-form").submit();
    },
    labels: {
      next: "Siguiente",
      previous: "Anterior",
      finish: "Cargar Imagenes"
    }
  });

  $('#publicarpor').change(function(event) {
    event.preventDefault();
    var _days = $( "#publicarpor option:selected").val();

    var now = new Date(Date.now() + 86400000 * _days);
    var _month = now.getMonth() + 1;
    var _newday = now.getDate() + '/' + _month + '/' + now.getFullYear();
    var _newday2 = now.getFullYear()+ '-' + _month + '-' + now.getDate();

    $(this).next('.info').html("Vence el " + _newday);
    $("#valido_hasta").attr('value', _newday2);

  });

  $('textarea#descripcion, textarea#comentario').maxlength({
    alwaysShow: true,
    threshold: 10,
    placement: 'bottom'
  });

  $('#photoimg').bind('change', function() {
      $("#imageform").ajaxForm({
        target: '#preview',
        beforeSubmit:function(){
          $("#imageloadstatus").show();
          $("#imageloadbutton").hide();
         },
        success:function(){
          //console.log('z');
          $("#imageloadstatus").hide();
          $("#imageloadbutton").show();
        },
        error:function(){
          //console.log('d');
          $("#imageloadstatus").hide();
          $("#imageloadbutton").show();
        }
      }).submit();

    });

  $( window ).load(function() {
    var wh = $(window).height();
    var dh = $(document).height();
    var ch = $('.completefooter').offset().top;

    // si el footer queda dentro del vieport le seteo el alto al footer
    if (wh == dh){
      var nh = wh - ch;
      $('.completefooter').height(nh);
    }

    $('#container').parent().height($('#container').height()+30);

    setTimeout($(".footer").show(), 1000);
    setTimeout($(".jsc-sidebar").show(), 1000);

  });

});
