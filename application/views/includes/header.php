<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <meta name="description" content="<?php echo $description; ?>">
  <meta name="keywords" content="paranarubros, parana, compra, venta, mercado, clasificado, rubro, Entre Rios, Santa Fe">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/favicon.png') ?>" />
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo base_url('assets/css/normalize.css') ?>" />
  <link rel='stylesheet' href="<?php echo base_url('assets/css/style.css') ?>" />
  <link rel='stylesheet' href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" />
  <link rel='stylesheet' href="<?php echo base_url('assets/css/lightbox.css') ?>" />

  <script src="<?php echo base_url('assets/js/vendor/modernizr-2.6.2.min.js') ?>"></script>

  <?php echo analytics_trackingcode('UA-53569302-1'); ?>
</head>

<body>
  <div class="wrapper jsc-sidebar-content jsc-sidebar-pulled">
		<div class="header">
		  <div class="headerwrapper">
		    <div class="logo">
          <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/images/logo.jpg') ?>" title="paranarubros.com.ar" /></a>
		    </div>
		    <div class="nav-icon">
		      <a href="#" class="right_bt jsc-sidebar-trigger" id="activator">Categor&iacute;as</a>
		    </div>
		    <div class="top-searchbar">
		      <form action="<?php echo base_url('buscar') ?>" method="POST">
		        <input type="text" name="buscar" id="buscar" />
            <input type="hidden" name="post" value="1" />
		        <input type="submit" value="" />
		      </form>
		    </div>
        <div class="userinfo">
          <?php if ($email=="") { ?>
            <a href="<?php echo base_url('usuario/login'); ?>" class="right_bt"><i class="fa fa-sign-in"></i><br>Login</a>
          <?php } else { ?>
            <a href="<?php echo base_url('usuario/logout'); ?>" class="right_bt"><i class="fa fa-sign-out"></i> Logout</a>
          <?php } ?>
		    </div>
		    <div class="box" id="box">
		      <ul>
		        <li><a href="<?php echo base_url(); ?>"><span>Inicio</span></a></li>
		        <!--li><a href="<?php echo base_url('nosotros'); ?>"><span>Nosotros</span></a></li-->
            <li><a href="<?php echo base_url('ayuda'); ?>"><span>Nosotros</span></a></li>
		        <li><a href="<?php echo base_url('ayuda'); ?>"><span>Ayuda</span></a></li>
		        <li><a href="<?php echo base_url('buscar'); ?>"><span>Buscar</span></a></li>
		        <li><a href="<?php echo base_url('usuario/micuenta'); ?>"><span>Mi Cuenta</span></a></li>
		        <li><a href="<?php echo base_url('contacto'); ?>"><span>Contacto</span></a></li>
            <li><a href="<?php echo base_url('usuario/publicar'); ?>" class="publicar"><span>Publicar un Aviso</span></a></li>
		        <div class="clear"> </div>
		      </ul>
		    </div>
		    <div class="clear"></div>
		  </div>
		</div>
