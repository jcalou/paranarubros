    <footer></footer>
    <script src="<?php echo base_url('assets/js/vendor/jquery-1.10.2.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/foundation.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/vendor/jquery.darktooltip.min.js') ?>"></script>
    <script>
      $(document).foundation();
    </script>

    <script>

      $(document).ready(function(){
        $('.confirm').darkTooltip({
          trigger:'click',
          animation:'flipIn',
          gravity:'south',
          confirm:true,
          yes:'Si',
          no:'No',
          onYes: function (data) {
            var _url = data.attr('data-user-id');
            window.location.href = "eliminar_usuario/"+_url;
          }
        });
      });

    </script>
  </body>
</html>
