<div class="contentb">
  <div id="container">
    <h1>Ayuda al Usuario</h1>
    <p>Bienvenido a nuestra secci&oacute;n de Ayuda, en esta p&aacute;gina encontrar&aacute;s respuestas a las preguntas que recibimos frecuentemente, si tu pregunta no est&aacute; enunciada en esta secci&oacute;n podes contactarnos a nuestro e-mail: <a class="common" href="mailto:info@paranarubros.com.ar">info@paranarubros.com.ar</a></p>
    <p><b>1. &iquest;Necesito registrarme para ver los avisos clasificados?</b></p>
    <p>No, no es requisito que est&eacute;s registrado para poder ver los avisos clasificados, sin embargo, es conveniente que te registres para poder disfrutar de todas las funcionalidades que brinda Paranarubros.</p>
    <p><b>2. &iquest;Por qu&eacute; debo registrarme?</b></p>
    <p>No es requisito que seas un usuario registrado en Paranarubros, sin embargo, nuestros usuarios registrados pueden acceder a prestaciones como publicar su clasificado y dem&aacute;s servicios que iremos incorporando.</p>
    <p><b>3. &iquest;C&oacute;mo me registro?</b></p>
    <p>El proceso de registro lo podes realizar haciendo <a class="common" href="<?php echo base_url('usuario/registro'); ?>">click Aqu&iacute;</a>, el registro es gratis y los datos son confidenciales, no dejes de completar los campos del formulario marcados como obligatorios.</p>
    <p><b>4. Olvid&eacute; mi clave &iquest;Como la recupero?</b></p>
    <p>Si olvidaste tu contrase&ntilde;a nos podes enviar un email a <a class="common" href="mailto:info@paranarubros.com.ar">info@paranarubros.com.ar</a> y te enviaremos una nueva.</p>
    <p><b>5. &iquest;C&oacute;mo publico un aviso?</b></p>
    <p>Haciendo click en el bot&oacute;n "Publicar un aviso" del &aacute;rea superior de la pantalla acceder&aacute;s al formulario de publicaci&oacute;n. Deber&aacute;s completar todos los campos marcados como obligatorios.</p>
    <p>Es altamente recomendable que cargues al menos una imagen en tu clasificado y coloques el precio de tu producto, ya que los avisos con fotos son los m&aacute;s visitados del portal.</p>
    <p><b>6. &iquest;Ya cargu&eacute; mi aviso, cuando aparecer&aacute; publicado en el sitio?</b></p>
    <p>En caso de seleccionar Rapipago o PagoFacil la acreditaci&oacute;n de los pagos demora aproximadamente 48hs. Si se paga con tarjeta de credito el aviso se publica instantaneamente.</p>
    <p><b>7. &iquest;Cu&aacute;nto cuesta publicar un aviso en Paranarubros?</b></p>
    <p>Paranarubros ofrece la publicaci&oacute;n de avisos clasificados con texto y fotograf&iacute;a del producto que desea vender disponible las 24 horas, los 365 d&iacute;as del a&ntilde;o, llegando a miles de personas.</p>
    <p>El valor de los avisos es de $25 cada 30 dias.</p>
    <p><b>8. &iquest;Cu&aacute;nto tiempo estar&aacute; en l&iacute;nea mi aviso?</b></p>
    <p>La vigencia de los avisos la selecciona al momento de cargar el producto, teniendo un m&iacute;nimo es de 150 d&iacute;as. Los Avisos que no sean renovados transcurridos el tiempo establecido para el rubro, ser&aacute;n dados de baja sin previo aviso.</p>
    <p><b>9. &iquest;Tengo otro problema que no est&aacute; enunciado en este listado, como puedo contactarme con uds.?</b></p>
    <p>Ante cualquier duda, sugerencia o inconveniente t&eacute;cnico por favor cont&aacute;ctate con nuestro servicio de informaci&oacute;n y asistencia. Por email: <a class="common" href="mailto:info@paranarubros.com.ar">info@paranarubros.com.ar</a>.</p>
    <p><b>10. &iquest;Por qu&eacute; no aparece mi aviso en p&aacute;gina principal?</b></p>
    <p>En la p&aacute;gina principal aparecen los &uacute;ltimos avisos cargados en el sistema.</p>
  </div>
</div>