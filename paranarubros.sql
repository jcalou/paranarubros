-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-04-2014 a las 23:00:27
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `paranarubros`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=36 ;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `url`) VALUES
(1, 'Autom&oacute;viles', 'automoviles'),
(2, 'Camionetas', 'camionetas'),
(3, 'N&aacute;utica', 'nautica'),
(4, 'Camionetas', 'camionetas'),
(5, 'N&aacute;utica', 'nautica'),
(6, 'Campos y Terrenos', 'camposyterrenos'),
(7, 'Casas', 'casas'),
(8, 'Construcci&oacute;n', 'construccion'),
(9, 'Departamentos', 'departamentos'),
(10, 'Fondos de Comercio', 'fondosdecomercio'),
(11, 'Locales, Galpones y Coch.', 'locales'),
(12, 'Quintas', 'quintas'),
(13, 'Audio y Video', 'audioyvideo'),
(14, 'Compras-Ventas Varias', 'varias'),
(15, 'Empleos', 'empleos'),
(16, 'Equip. para negocios', 'negocios'),
(17, 'Fotograf&iacute;a', 'fotografia'),
(18, 'Inform&aacute;tica', 'informatica'),
(19, 'Instrumentos Musicales', 'instrumentos'),
(20, 'Telefon&iacute;a', 'telefonia'),
(21, 'Agropecuarios', 'agropecuarios'),
(22, 'Amoblamientos', 'amoblamientos'),
(23, 'Animaci&oacute;n de Fiestas', 'animaciondefiestas'),
(24, 'Antigüedades', 'antiguedades'),
(25, 'Cursos / Ense&ntilde;anzas', 'cursos'),
(26, 'Deportes', 'deportes'),
(27, 'Indumentaria', 'indumentaria'),
(28, 'M&aacute;quinas y Herramientas', 'maquinas'),
(29, 'Mascotas', 'mascotas'),
(30, 'Plantas', 'plantas'),
(31, 'Profesionales', 'profesionales'),
(32, 'Salud / Belleza', 'salud'),
(33, 'Servicios Varios', 'serviciosvarios'),
(34, 'Solidaridad/Donaciones', 'solidaridad'),
(35, 'Turismo', 'turismo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE IF NOT EXISTS `imagenes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_publicacion` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `principal` char(1) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion`
--

CREATE TABLE IF NOT EXISTS `publicacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL DEFAULT '0',
  `id_categoria` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `descripcion` varchar(400) COLLATE latin1_spanish_ci DEFAULT NULL,
  `precio` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `forma_de_pago` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `comentario` varchar(400) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fecha_de_carga` date DEFAULT NULL,
  `valido_hasta` date DEFAULT NULL,
  `estado` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `valor` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `usuario_email` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `usuario_domicilio` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `usuario_tel_fijo` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `usuario_tel_movil` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `password` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tel_fijo` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tel_movil` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `domicilio` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `localidad` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cod_post` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `provincia` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `password`, `tel_fijo`, `tel_movil`, `domicilio`, `localidad`, `cod_post`, `provincia`) VALUES
(3, 'Juan Calou', 'juan@mail.com', '202cb962ac59075b964b07152d234b70', '47350325', '55150297', 'San Martin 334', 'Villaguay', '6546', 'Entre Rios');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
