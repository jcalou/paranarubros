<div class="row">
  <div class="large-12 columns">
    <h1>Editar usuario</h1>
    <?= validation_errors(); ?>
    <form action="<?php echo base_url('admin/editar_usuario'); ?>" method="POST">
      <div class="row">
        <div class="large-2 columns">
          <label for="nombre">Nombre:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="nombre" value="<?= set_value('nombre',$usuario['nombre']);?>" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="email">Email:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="email" value="<?= set_value('email',$usuario['email']);?>" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="password">Password:</label>
        </div>
        <div class="large-8 columns">
          <input type="password" name="password" value="<?= set_value('password',$usuario['password']);?>" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="tel_fijo">Tel&eacute;fono Fijo:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="tel_fijo" value="<?= set_value('tel_fijo',$usuario['tel_fijo']);?>" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="tel_movil">Tel&eacute;fono M&oacute;vil:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="tel_movil" value="<?= set_value('tel_movil',$usuario['tel_movil']);?>" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="domicilio">Domicilio:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="domicilio" value="<?= set_value('domicilio',$usuario['domicilio']);?>" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="localidad">Localidad:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="localidad" value="<?= set_value('localidad',$usuario['localidad']);?>" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="cod_post">C&oacute;digo postal:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="cod_post" value="<?= set_value('cod_post',$usuario['cod_post']);?>" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-2 columns">
          <label for="provincia">Provincia:</label>
        </div>
        <div class="large-8 columns">
          <input type="text" name="provincia" value="<?= set_value('provincia',$usuario['provincia']);?>" />
        </div>
        <div class="large-2 columns">&nbsp;</div>
      </div>
      <div class="row">
        <div class="large-10 columns">
          <input type="hidden" name="post" value="1" />
          <input type="submit" class="button right" value="Guardar cambios"/>
        </div>
      </div>
    </form>
  </div>
</div>
