<?php
$mp = new MP("3183534982022887", "7hC6MG78CihCYA0w6VZdntAHiy0Gqua0"); // user Natalio Nuesch
$mp->sandbox_mode(TRUE); //descomentar para cobrar en serio

switch ($publicacion['cantidad_dias']) {
  case "30":
    $precio = 25;
    break;
  case "60":
    $precio = 50;
    break;
  case "90":
    $precio = 75;
    break;
  case "120":
    $precio = 100;
    break;
  case "150":
    $precio = 125;
    break;
  default:
    $precio = 25;
}

$preference = array(
    "items" => array(
        array(
            "title" => "Paranarubros: " + $publicacion['titulo'],
            "quantity" => 1,
            "currency_id" => "ARS",
            "unit_price" => $precio
        )
    )
);

$preferenceResult = $mp->create_preference($preference);
?>
<div class="contentb">
  <div id="container">
    <h1>Pagar publicaci&oacute;n</h1>
    <?php if ($publicacion['estado'] != "activo"){?>
      <p>Clickee en el boton Pagar y se abrira una ventana con las opciones de pago.</p>
      <a href="<?php echo $preferenceResult["response"]["sandbox_init_point"]; ?>" name="MP-Checkout" class="lightblue-L-Sq-Ar-ArOn" mp-mode="modal" onreturn="execute_my_onreturn">Pagar</a>
    <?php } else {?>
      <p>Su publicacion esta activa y vence el dia <?php echo date('d-m-Y', strtotime($publicacion['valido_hasta']))?>.</p>
    <?php } ?>
    <script type="text/javascript" src="http://mp-tools.mlstatic.com/buttons/render.js"></script>

    <!-- Abre el Checkout -->
    <script type="text/javascript">
    function execute_my_onreturn (json) {
      if (json.collection_status=='approved'){
        //alert ("Publicacion $publicacion['id'] pagada");
        // activar publicacion $id_publicacion
        //window.location = "/paranarubros/usuario/activar_publicacion/<?php echo $publicacion['id']; ?>";
        window.location = "/usuario/activar_publicacion/<?php echo $publicacion['id']; ?>";
      } else if(json.collection_status=='pending'){
        alert ('Por el tipo de pago que eligio su pago esta siendo procesado.');
        //window.location = "/paranarubros/usuario/procesando_pago/<?php echo $publicacion['id']; ?>";
        window.location = "/usuario/procesando_pago/<?php echo $publicacion['id']; ?>";
      } else if(json.collection_status=='in_process'){
        alert ('Por el tipo de pago que eligio su pago esta siendo procesado.');
        //window.location = "/paranarubros/usuario/procesando_pago/<?php echo $publicacion['id']; ?>";
        window.location = "/usuario/procesando_pago/<?php echo $publicacion['id']; ?>";
      } else if(json.collection_status=='rejected'){
        alert ('Su pago fue rechazado, por favor intente nuevamente.');
      } else if(json.collection_status==null){
        alert ('No se completó el proceso de pago, no se ha generado ningún pago.');
      }
    }
    </script>
  </div>
</div>
