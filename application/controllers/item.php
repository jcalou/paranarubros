<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Item extends CI_Controller
{
    public function __construct()
    {
        //Cargamos El Constructor
        parent::__construct();
        $this->load->model('publicacion_model');
        }

    public function index($id)
    {
        $data['title'] = 'Paranarubros.com.ar - Descripci&oacute;n de la publicaci&oacute;n';
        $data['description'] = 'Paranarubros.com.ar - Descripci&oacute;n de la publicaci&oacute;n';

        if($this->session->userdata('logged_in')) {
          $session_data = $this->session->userdata('logged_in');
          $data['email'] = $session_data['email'];
        }else{
          $data['email'] = "";
        }

        $data['item_id'] = $id;
        $data['item'] = $this->publicacion_model->get_publicacion($id);
        $data['imagenes'] = $this->publicacion_model->get_imagenes($id);        

        if ($data['item'] != false) {
          $this->load->view('includes/header', $data);
          $this->load->view('item', $data);
          $this->load->view('includes/footer');
          $this->load->view('includes/categories_nav');
        }else{
          redirect('/index');
          return false;
        }
    }

}
