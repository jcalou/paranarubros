<div class="contentb">
  <div id="container">
    <h1>Registro</h1>
    <div class="form_errors">
      <?php echo validation_errors(); ?>
    </div>
    <form action="<?php echo base_url('usuario/registro') ?>" method="POST">
      <label for="email">Nombre:</label>
      <input type="text" size="20" id="nombre" name="nombre"/>
      <div class="info">Ingrese su nombre completo.</div>
      <br/>
      <label for="email">Email:</label>
      <input type="text" size="20" id="email" name="email"/>
      <div class="info">Ingrese su email. &Eacute;ste ser&aacute; usado como nombre de usuario.</div>
      <br/>
      <label for="password">Contrase&ntilde;a:</label>
      <input type="password" size="20" id="passowrd" name="password"/>
      <div class="info">Ingrese su contrase&ntilde;a.</div>
      <br/>
      <label for="passwordconf">Confirmar contrase&ntilde;a:</label>
      <input type="password" size="20" id="passwordconf" name="passwordconf"/>
      <div class="info">Repita la misma contrase&ntilde;a.</div>
      <br/>
      <label for="tel_fijo">Tel&eacute;fono Fijo:</label>
      <input type="text" size="20" id="tel_fijo" name="tel_fijo" placeholder="(0343) XXX-XXXX" />
      <div class="info">Ingrese su n&uacute;mero de tel&eacute;fono fijo.</div>
      <br/>
      <label for="tel_movil">Tel&eacute;fono M&oacute;vil:</label>
      <input type="text" size="20" id="tel_movil" name="tel_movil" placeholder="(0343) 15 XXX-XXXX" />
      <div class="info">Ingrese su n&uacute;mero de tel&eacute;fono m&oacute;vil.</div>
      <br/>
      <label for="domicilio">Domicilio:</label>
      <input type="text" size="20" id="domicilio" name="domicilio"/>
      <div class="info">Ingrese su domicilio.</div>
      <br/>
      <label for="localidad">Localidad:</label>
      <input type="text" size="20" id="localidad" name="localidad" placeholder="Paran&aacute;"/>
      <div class="info">Ingrese su localidad.</div>
      <br/>
      <label for="cod_post">C&oacute;digo Postal:</label>
      <input type="text" size="20" id="cod_post" name="cod_post" placeholder="3100"/>
      <div class="info">Ingrese su c&oacute;digo postal.</div>
      <br/>
      <label for="provincia">Provincia:</label>
      <input type="text" size="20" id="provincia" name="provincia" placeholder="Entre R&iacute;os"/>
      <div class="info">Ingrese su provincia.</div>
      <br/>
      <input type="hidden" name="post" value="1" />
      <input type="submit" value="Registrarse"/>
    </form>

    <p class="form-bottom-link">¿Ya estas registrado? <a href="<?php echo base_url('usuario/login') ?>">Login</a></p>
    <p class="form-bottom-link">Los datos cargados son confidenciales. Al publicar un aviso ud. podrá seleccionar los datos que desea mostrar para que lo contacten los interesados.</p>
  </div>
</div>
