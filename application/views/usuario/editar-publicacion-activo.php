<div class="contentb">
  <div id="container">
    <h1>Editar publicacion</h1>
    <div class="form_errors">
      <?php echo validation_errors(); ?>
    </div>
    <form action="<?php echo base_url('usuario/editar_publicacion_activo/'.$id_publicacion) ?>" method="POST">
      <?php
          if (isset($usuario_logueado['email']) && $usuario_logueado['email'] != null){
            if (isset($publicacion['usuario_email']) && $publicacion['usuario_email'] != "") {
              $checked = "checked";
            } else {
              $checked = "";
            }
            echo "<div class='checkboxwrapper'><p><b>Email:</b> ".$usuario_logueado['email']."<input name='pubmail' type='checkbox' id='pubmail' value='true' ".$checked." /><span class='form-check-text'>Deseo que se muestre mi email en la publicaci&oacute;n.</span></p></div>";
          }
          if (isset($usuario_logueado['tel_fijo']) && $usuario_logueado['tel_fijo'] != null){
            if (isset($publicacion['usuario_tel_fijo']) && $publicacion['usuario_tel_fijo'] != "") {
              $checked = "checked";
            } else {
              $checked = "";
            }
            echo "<div class='checkboxwrapper'><p><b>Tel&eacute;fono Fijo:</b> ".$usuario_logueado['tel_fijo']."<input name='pubtel_fijo' type='checkbox' id='pubtel_fijo' value='true' ".$checked." /><span class='form-check-text'>Deseo que se muestre mi tel&eacute;fono fijo en la publicaci&oacute;n.</span></p></div>";
          }
          if (isset($usuario_logueado['tel_movil']) && $usuario_logueado['tel_movil'] != null){
            if (isset($publicacion['usuario_tel_movil']) && $publicacion['usuario_tel_movil'] != "") {
              $checked = "checked";
            } else {
              $checked = "";
            }
            echo "<div class='checkboxwrapper'><p><b>Tel&eacute;fono M&oacute;vil:</b> ".$usuario_logueado['tel_movil']."<input name='pubtel_movil' type='checkbox' id='pubtel_movil' value='true' ".$checked." /><span class='form-check-text'>Deseo que se muestre mi tel&eacute;fono m&oacute;vil en la publicaci&oacute;n.</span></p></div>";
          }
          if (isset($usuario_logueado['domicilio']) && $usuario_logueado['domicilio'] != null){
            if (isset($publicacion['usuario_domicilio']) && $publicacion['usuario_domicilio'] != "") {
              $checked = "checked";
            } else {
              $checked = "";
            }
            echo "<div class='checkboxwrapper'><p><b>Domicilio:</b> ".$usuario_logueado['domicilio']."<input name='pubdomicilio' type='checkbox' id='pubdomicilio' value='true' ".$checked." /><span class='form-check-text'>Deseo que se muestre mi domicilio en la publicaci&oacute;n.</span></p></div>";
          }
          ?>

            <label for="titulo">T&iacute;tulo:</label>
            <input type="text" size="20" id="titulo" name="titulo" class="required" value="<?=$publicacion['titulo']?>" />
            <div class="info">Ingrese el t&iacute;tulo para la publicaci&oacute;n.</div>

            <label for="descripcion">Descripci&oacute;n:</label>
            <textarea name="descripcion" id="descripcion" maxlength="400" cols="30" rows="10"><?=$publicacion['descripcion']?></textarea>
            <div class="info">Ingrese la descripci&oacute;n para la publicaci&oacute;n. M&aacute;ximo 400 caracteres.</div>

            <label for="precio">Precio:</label>
            <input type="text" size="20" id="precio" name="precio" value="<?=$publicacion['precio']?>" />
            <div class="info">Ingrese el precio que desea aplicarle al producto publicado.</div>

            <label for="formapago">Forma de pago:</label>
            <input type="text" size="20" id="formapago" name="formapago" placeholder="Contado, cuotas, a convenir..." value="<?=$publicacion['forma_de_pago']?>" />
            <div class="info">Ingrese el forma de pago para la publicaci&oacute;n.</div>

            <label for="comentario">Comentarios adicionales:</label>
            <textarea name="comentario" id="comentario" maxlength="400" cols="30" rows="10"><?=$publicacion['comentario']?></textarea>
            <div class="info">Ingrese comentarios adicionales que quisiera incluir en la publicaci&oacute;n. M&aacute;ximo 400 caracteres.</div>


      <input type="hidden" name="post" value="1" />
      <input type="submit" value="Guardar" />
    </form>
    <p>&nbsp;</p>
  </div>
</div>
